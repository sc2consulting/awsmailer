<!-- View stored in resources/views/campaign/detail.blade.php -->
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>AwsMailer - Campaign</title>

    <link href = {{ asset("bootstrap/css/bootstrap.css") }} rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="{{ asset('css/base.css') }}" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<script>
$(document).ready(function(){
	// Activate tooltip
	$('[data-toggle="tooltip"]').tooltip();
	
	// Select/Deselect checkboxes
	var checkbox = $('table tbody input[type="checkbox"]');
	$("#selectAll").click(function(){
		if(this.checked){
			checkbox.each(function(){
				this.checked = true;                        
			});
		} else{
			checkbox.each(function(){
				this.checked = false;                        
			});
		} 
	});
	checkbox.click(function(){
		if(!this.checked){
			$("#selectAll").prop("checked", false);
		}
	});
});
</script>
</head>
<body>

@extends('layouts.navbar')

@section('content')

<div class="container-xl">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-sm-6">
						<h2>Détail <b>Campagnes {{ $campaign['id'] }}</b></h2>
					</div>
				</div>
			</div>
			<table class="table table-striped table-hover">
				<thead>
					<tr>

						<th>ID</th>
						<th>Name</th>
                        <th>Sender</th>
						<th>Objet</th>
                        <th>Fichier HTML</th>
                        <th>Active</th>
					</tr>
				</thead>
				<tbody>
                
					<tr>
                        <td>{{ $campaign['id'] }}</td>
                        <td>{{ $campaign['name'] }}</td>
                        <td>{{ $campaign['sender'] }}</td>
                        <td>{{ $campaign['subject'] }}</td>
                        <td>{{ $campaign['htmlFile'] }}</td>
                        <td>{{ $campaign['is_active'] }}</td>
                        
					</tr>
                     
				</tbody>
			</table>
        </div>
	</div>        
</div>
<div class="container-xl">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-sm-6">
						<h2>Stats <b>Campagnes {{ $campaign['id'] }}</b></h2>
					</div>
				</div>
			</div>
            <table class="table table-striped table-hover">
                <thead>
                     <tr>
                        <th>name</th>
                        <th>sent</th>
                        <th>delivery</th>
                        <th>open</th>
                        <th>click</th>
                        <th>bounce</th>
                        <th>complaints</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($stats as $stat)
                        <tr>
                            <td> {{ $stat['name'] }} </td>
                            <td> {{ $stat['sent'] }} </td>
                            <td> {{ $stat['delivery'] }} </td>
                            <td> {{ $stat['open'] }} </td>
                            <td> {{ $stat['click'] }} </td>
                            <td> {{ $stat['bounce'] }} </td>
                            <td> {{ $stat['complaint'] }} </td>
                        </tr>
                        @endforeach
                </tbody>
	        </table>
	    </div>        
	</div>
	@endsection
</body>
</html>
