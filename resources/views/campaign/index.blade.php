<!-- View stored in resources/views/campaign.index.blade.php -->
<!DOCTYPE html>
<html lang="fr">
<head>
	<title>AwsMailer - Campaign</title>

	<link href = {{ asset("bootstrap/css/bootstrap.css") }} rel="stylesheet" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="{{ asset('css/base.css') }}" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
	<script>
		function showAddRecipientForm(elem, campaign_id) {
			let request = new XMLHttpRequest();
			request.onreadystatechange = function () {
				if (request.readyState === 4 && request.status === 200) {
					document.getElementById('addRecipientFormBody').innerHTML = request.responseText;
				}
			}
			let modal = document.getElementById("addRecipientForm");
			modal.style.display = "block";
			request.open("GET", "/campaign/addRecipientForm?campaign_id=" + campaign_id, true);
			request.send(null);
		}

		function closeAddRecipientForm() {
			document.getElementById('addRecipientFormBody').innerHTML = "<div class=\"loader\"></div>";
			let modal = document.getElementById("addRecipientForm");
			modal.style.display = "none";
		}
	</script>
	<style>
		.clickable {
			cursor: pointer;
		}
		/* The Modal (background) */
		.form-modal {
			display: none; /* Hidden by default */
			position: fixed; /* Stay in place */
			z-index: 1; /* Sit on top */
			width: 600px;
			height: 280px;
			left: 50%;
			top: 50%;
			margin-left: -300px;
			margin-top: -300px;
			overflow: auto; /* Enable scroll if needed */
			background-color: rgb(0,0,0); /* Fallback color */
			background-color: rgba(200,200,200,1); /* Black w/ opacity */
		}

		/* Modal Content/Box */
		.modal-content {
			background-color: #fefefe;
			margin: 15% auto; /* 15% from the top and centered */
			padding: 20px;
			border: 1px solid #888;
			width: 80%; /* Could be more or less, depending on screen size */
		}

		/* The Close Button */
		.close {
			color: #aaa;
			float: right;
			font-size: 28px;
			font-weight: bold;
		}

		.close:hover,
		.close:focus {
			color: black;
			text-decoration: none;
			cursor: pointer;
		}


		.loader {
			border: 16px solid #f3f3f3; /* Light grey */
			border-top: 16px solid #3498db; /* Blue */
			border-radius: 50%;
			width: 120px;
			height: 120px;
			animation: spin 2s linear infinite;
		}

		@keyframes spin {
			0% { transform: rotate(0deg); }
			100% { transform: rotate(360deg); }
		}


	</style>
</head>
<body>
@extends('layouts.navbar')

@section('content')

	<div class="container-xl">
		<div class="table-responsive">
			<div class="table-wrapper">
				<div class="table-title">
					<div class="row">
						<div class="col-sm-6">
							<h2>Listes <b>Campagnes</b></h2>
						</div>
						<div class="col-sm-6">
							<a href="/campaign/create" class="btn btn-success"><i class="material-icons">&#xE147;</i> <span>Add New Campagne</span></a>
						</div>
					</div>
				</div>
				<!-- ADD Recipient -->
				<div class="form-modal" id="addRecipientForm">
					<form action="/campaign/addRecipient" method="POST" enctype="multipart/form-data">
						@csrf

						<div class="modal-header">
							<h4 class="modal-title">Add Recipient</h4>
							<button type="button" class="close" onclick="closeAddRecipientForm()">&times;</button>
						</div>

						<div class="modal-body" id="addRecipientFormBody">
							<div class="loader"></div>
						</div>

						<p style="padding-left: 10px"> note : If an existing file is selected, the import of a new list will be ignored</p>
						<div class="modal-footer">
							<input type="button" class="btn btn-default" onclick="closeAddRecipientForm()" value="Cancel">
							<input type="submit" class="btn btn-info" value="Add">
						</div>
					</form>
				</div>

				<table class="table table-striped table-hover">
					<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Objet</th>
						<th>emails in queue</th>
						<th>Actions</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($campaignList as $campaign)
						<tr>
							<td>{{ $campaign->id }}</td>
							<td>{{ $campaign->name }}</td>
							<td>{{ $campaign->mail_subject }}</td>
							<td>{{ $pending_list[$campaign->id] }}</td>
							<td>
								<!-- <a href="#editCampagneModal" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a> -->
								<a href="/campaign/detail?id={{ $campaign->id }}"><i class="material-icons" data-toggle="tooltip" title="Detail">&#xe8f4;</i></a>
								<a href="#testsend{{ $campaign->id }}" class="send" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Test">&#xe9d8;</i></a>
								<i class="material-icons clickable" onclick="showAddRecipientForm(this, {{ $campaign->id }})">&#xef65;</i>

								@if($campaign->email_sent == 0)
									<a href="#deleteCampagneModal{{ $campaign->id }}" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
								@endif
							</td>
						<!-- test Campaign -->
							<td>
								<div id="testsend{{ $campaign->id }}" class="modal fade">
									<div class="modal-dialog">
										<div class="modal-content">
											<form action="/campaign/testCampaign" method="POST" enctype="multipart/form-data">
												@csrf
												<input name="campaign_id" type="hidden" value="{{ $campaign->id }}">
												<div class="modal-header">
													<h4 class="modal-title">Add Recipient</h4>
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
												</div>
												<div class="modal-body">
												<!--<div class="form-group">
										<label for="inputState">Campagne ID</label>
										<select id="inputState" class="form-control">
											<option selected>{{ $campaign->id }}</option>
										</select>
									</div>-->
													<div class="form-group">
														<label>Email</label>
														<input type="email" name="email" id="email" class="form-control" required>
													</div>
													<div class="modal-footer">
														<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
														<input type="submit" class="btn btn-info" value="Add">
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</td>
							<!-- Delete Campaign -->
							@if($campaign->email_sent == 0)
								<td>
									<div id="deleteCampagneModal{{ $campaign->id }}" class="modal fade">
										<div class="modal-dialog">
											<div class="modal-content">
												<form method="POST" action="/campaign/delete">
													@csrf
													<input id="" name="campaign_id" type="hidden" value="{{ $campaign->id }}">
													<div class="modal-header">
														<h4 class="modal-title">Delete Campagne</h4>
														<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													</div>
													<div class="modal-body">
														<p>Êtes vous sure de vouloir supprimer la campagne ?</p>
														<p class="text-warning"><small>Cette action sera irréversible .</small></p>
													</div>
													<div class="modal-footer">
														<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
														<input type="submit" class="btn btn-danger" value="Delete"  >
													</div>
												</form>
											</div>
										</div>
									</div>
								</td>
							@endif
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
</body>
</html>
