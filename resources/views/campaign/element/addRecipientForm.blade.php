<div class="form-group">
    <input name="campaign_id" type="hidden" value="{{ $campaign_id }}">

    <label for="existingFile">Select existing file</label>
    <select id="existingFile" name="existingFile">
        <option value="none">Select an existing file</option>

        @foreach ($files as $file)
            @if(!is_dir($file))
                <option value="{{ $file }}">{{ $file }}</option>
            @endif
        @endforeach
    </select>
    <div id="new-file">
        <input type="file"  name="mail_file">
    </div>
</div>
