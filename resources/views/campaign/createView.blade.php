<!-- View stored in resources/views/campaign/detail.blade.php -->
<!DOCTYPE html>
<html lang="fr">
<head>
    <title>AwsMailer - Campaign</title>

    <link href = {{ asset("bootstrap/css/bootstrap.css") }} rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="{{ asset('css/base.css') }}" rel="stylesheet">
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

<script>
$(document).ready(function(){
	// Activate tooltip
	$('[data-toggle="tooltip"]').tooltip();
	
	// Select/Deselect checkboxes
	var checkbox = $('table tbody input[type="checkbox"]');
	$("#selectAll").click(function(){
		if(this.checked){
			checkbox.each(function(){
				this.checked = true;                        
			});
		} else{
			checkbox.each(function(){
				this.checked = false;                        
			});
		} 
	});
	checkbox.click(function(){
		if(!this.checked){
			$("#selectAll").prop("checked", false);
		}
	});
	$("input[type=file]").change(function (e){$(this).next('.custom-file-label').text(e.target.files[0].name);})

});
</script>
</head>
<body>

@extends('layouts.navbar')

@section('content')
<div class="container-xl">
	<div class="table-responsive">
		<div class="table-wrapper">
			<div class="table-title">
				<div class="row">
					<div class="col-sm-6">
						<h2>Crée <b>Campagne</b></h2>
					</div>
				</div>
            </div>
            
            <div class="modal-body">
                <form action="/campaign/create" method="POST" enctype="multipart/form-data">
				@csrf				
					<div class="form-group" >
						<label>Name</label>
						<input type="text" name ="name" id="name" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Sender ID</label>
						<input type="text" name ="sender_id" id="sender_id" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Sender Name</label>
						<input type="text" name ="sender_name" id="sender_name" class="form-control" required>
					</div>
					<div class="form-group">
						<label>Mail Subject</label>
						<textarea type="text" name ="mail_subject" id="mail_subject" class="form-control" required></textarea>
					</div>
					<div class="custom-file">
						<input type="file" class="custom-file-input" name="mail_html" accept=".html">
						<label class="custom-file-label" for="file">Sélectionner un fichier</label>
					</div>
                <div class="modal-footer">
					<button type="submit" class="btn btn-default" style="background-color:#435d7d;color:white;">Submit</button>
                </div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
</body>
</html>
