<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PlaningDateToDatetime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('campaign_planning', function (Blueprint $table) {
		    $table->dropColumn('sending_date');
	    });

	    Schema::table('campaign_planning', function (Blueprint $table) {
		    $table->dateTime('sending_date')->after('campaign_id')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
