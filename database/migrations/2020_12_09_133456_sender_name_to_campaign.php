<?php

use App\Models\Campaign;
use App\Models\Sender;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SenderNameToCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign', function (Blueprint $table) {
            $table->string('sender_name')->after('sender_id');
        });

        $campaigns = Campaign::all();
        foreach ($campaigns as $campaign) {
        	$campaign_sender = Sender::where('id', $campaign->sender_id)->first();
        	$campaign->sender_name = $campaign_sender->name;
        	$campaign->save();
        }

        Schema::table('sender', function (Blueprint $table) {
        	$table->dropColumn('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('sender', function (Blueprint $table) {
    		$table->string('name');
	    });

    	$old_senders = Sender::all();

	    $campaigns = Campaign::all();
	    foreach ($campaigns as $campaign) {
	    	$new_sender = new Sender;
		    $campaign_sender = Sender::where('id', $campaign->sender_id)->first;
		    $new_sender->name = $campaign->sender_name;
		    $new_sender->email = $campaign_sender->email;
		    $new_sender->save();
		    $campaign->sender_id = $new_sender->id;
	    }

	    foreach ($old_senders as $old_sender) {
		    $old_sender->delete();
	    }

	    Schema::table('campaign', function (Blueprint $table) {
            $table->dropColumn('sender_name');
        });
    }
}
