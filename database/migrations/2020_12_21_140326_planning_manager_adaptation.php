<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PlanningManagerAdaptation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign_planning', function (Blueprint $table) {
	        $table->dropColumn('recipient_count');
	        $table->dropColumn('sent_count');
	        $table->dropColumn('planning_id');
        });
	    Schema::table('campaign_planning', function (Blueprint $table) {
        	$table->integer('campaign_id');
        	$table->boolean('active')->default(true);
        	$table->integer('sent_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
