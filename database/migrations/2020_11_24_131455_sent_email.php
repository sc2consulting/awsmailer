<?php

use App\Models\Campaign;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SentEmail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sent_email', function (Blueprint $table) {
        	$table->id();
        	$table->string('email');
        	$table->integer('campaign_id');
        	$table->string('aws_message_id');
        	$table->integer('fai_id');
	        $table->timestamps();
        });

        Schema::table('recipient_emails', function (Blueprint $table) {
        	$table->integer('delivered')->default(0);
        });

        Schema::table('campaign', function (Blueprint $table) {
        	$table->integer('delivery')->default(0);
        	$table->integer('open')->default(0);
        	$table->integer('click')->default(0);
        });

        $campaigns = Campaign::all();
        $keyList = ['delivery', 'complaint', 'bounce', 'open', 'click', 'sent'];
        foreach ($campaigns as $campaign) {
        	if ($campaign->fai_stats !== NULL) {
		        $fai_stats = json_decode($campaign->fai_stats, true);
		        $fai_id_list = array_keys($fai_stats);
		        foreach ($fai_id_list as $fai_id) {
		        	foreach ($keyList as $key) {
				        if (!key_exists($key, $fai_stats[$fai_id])) {
				        	$fai_stats[$fai_id][$key] = 0;
				        }
			        }
		        }
		        $campaign->fai_stats = $fai_stats;
		        $campaign->save();
	        }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
