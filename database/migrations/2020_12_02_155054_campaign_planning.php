<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CampaignPlanning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_planning', function (Blueprint $table) {
        	$table->id();
        	$table->date('sending_date');
        	$table->integer('recipient_count')->default(0);
        	$table->integer("planning_id");
        	$table->integer('sent_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
