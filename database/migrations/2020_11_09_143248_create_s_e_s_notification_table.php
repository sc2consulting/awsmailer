<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSESNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ses_notification', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->integer('campaign_id')->nullable();
			$table->integer('fai_id');
			$table->string('message_id');

			$table->string('status');
	        $table->string('status_type')->nullable();
	        $table->string('status_sub_type')->nullable();
	        $table->string('smtp_response')->nullable();

	        $table->dateTime('notified_at');
	        $table->dateTime('arrivalDate')->nullable();

	        $table->json('others')->nullable();
            $table->timestamps();
        });

	    Schema::create('fai_list', function (Blueprint $table) {
			$table->id();
			$table->string('name');
			$table->string('ndd');
			$table->timestamps();
	    });

	    Schema::create('campaign', function (Blueprint $table) {
		    $table->id();
		    $table->string('name');
		    $table->integer('sender_id');
		    $table->string('mail_subject');
		    $table->string('mail_body');
		    $table->integer('email_sent')->default(0);
		    $table->integer('complaints')->default(0);
		    $table->integer('bounces')->default(0);
		    $table->timestamps();
	    });

	    Schema::create('sender', function (Blueprint $table) {
	    	$table->id();
	    	$table->string('name');
	    	$table->string('email');
	    	$table->string('aws_id');
	    	$table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
