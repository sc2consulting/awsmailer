<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EmailList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('recipient_emails', function (Blueprint $table) {
		    $table->id();
		    $table->string('email');
		    $table->integer('fai_id');
		    $table->boolean('unsubscribe')->default(false);
		    $table->dateTime('last_sent')->nullable();
		    $table->json('campaign_id_list')->nullable();

		    $table->integer('sent_emails')->default(0);
		    $table->integer('open')->default(0);
		    $table->integer('click')->default(0);
		    $table->integer('bounces')->default(0);
		    $table->integer('complaints')->default(0);

		    $table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::drop('recipient_email');
    }
}
