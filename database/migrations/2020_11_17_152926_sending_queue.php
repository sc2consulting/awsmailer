<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SendingQueue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('sending_queue', function (Blueprint $table) {
		    $table->id();
		    $table->string('email');
		    $table->integer('fai_id');
		    $table->integer('campaign_id');

		    $table->timestamps();
        });

	    Schema::table('campaign', function (Blueprint $table) {
	    	$table->boolean('isActive')->default(false);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sending_queue');
    }
}
