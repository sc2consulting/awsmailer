<?php


namespace Database\Seeders;

use App\Models\FaiList;
use App\Models\Sender;
use Illuminate\Database\Seeder;

class FaiListTableSeeder extends Seeder
{
	/**
	 * generate list of Fai and associated ndd
	 *
	 * @return void
	 */
	public function run()
	{
		FaiList::create(['id' => 1, 'name' => 'orange', 'ndd' => 'orange.fr|wanadoo.fr|voila.fr']);
		FaiList::create(['id' => 2, 'name' => 'free', 'ndd' => 'free.fr|freesbee.fr|libertysurf.fr|worldonline.fr|online.fr|alicepr.fr|alicead],.fr|alicemail.fr|infonie.fr']);
		FaiList::create(['id' => 3, 'name' => 'gmail', 'ndd' => 'gmail.com']);
		FaiList::create(['id' => 4, 'name' => 'sfr',  'ndd' =>'sfr.fr|neuf.fr|cegetel.net|club-internet.fr|numericable.fr|numericabl.com|no],.fr|neufcegetel.fr']);
		FaiList::create(['id' => 5, 'name' => 'laposte', 'ndd' => 'laposte.net|laposte.fr']);
		FaiList::create(['id' => 6, 'name' => 'autre', 'ndd' => 'blabla.com']);
		FaiList::create(['id' => 7, 'name' => 'hotmail', 'ndd' => 'hotmail.fr|msn.fr|live.fr|outlook.fr|outlook.com|msn.com']);
		FaiList::create(['id' => 8, 'name' => 'yahoo', 'ndd' => 'yahoo.fr|yahoo.com']);
		FaiList::create(['id' => 9, 'name' => 'aol',  'ndd' =>'aol.com|aol.fr']);
		FaiList::create(['id' => 10,'name' =>  'bbox', 'ndd' => 'bbox.fr']);
		FaiList::create(['id' => 11,'name' =>  'apple', 'ndd' => 'icloud.com']);
		FaiList::create(['id' => 12,'name' =>  'autrefr', 'ndd' => 'autrefr']);
		FaiList::create(['id' => 13,'name' =>  'suisse', 'ndd' => 'adressesuisse']);
		FaiList::create(['id' => 14,'name' =>  'belgique', 'ndd' => 'adressebelgique']);
		FaiList::create(['id' => 15,'name' =>  'tiscali', 'ndd' => 'tiscali.fr']);
	}
}

