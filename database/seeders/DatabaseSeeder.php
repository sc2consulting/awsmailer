<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//		$this->call('FaiListTableSeeder');
//	    $this->command->info('Fai List table seeded!');
		echo "run 'php artisan db:seed --class=FaiListTableSeeder '";
    }
}
