<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Installer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'awsMailer:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	/*
    	 * Storage Paths
    	 */

	    // campaign
	    if (!file_exists(storage_path(). "/campaign/")) {
		    mkdir(storage_path(). "/campaign/");
		    echo "created " . storage_path(). "/campaign/ folder." . PHP_EOL;
	    }
	    if (!file_exists(storage_path(). "/campaign/html/")) {
		    mkdir(storage_path(). "/campaign/html/");
		    echo "created " . storage_path(). "/campaign/html/ folder." . PHP_EOL;
	    }
	    if (!file_exists(storage_path(). "/campaign/mailList/")) {
		    mkdir(storage_path(). "/campaign/mailList/");
		    echo "created " . storage_path(). "/campaign/mailList/ folder." . PHP_EOL;
	    }
	    if (!file_exists(storage_path(). "/campaign/unsubList/")) {
		    mkdir(storage_path(). "/campaign/unsubList/");
		    echo "created " . storage_path(). "/campaign/unsub/ folder." . PHP_EOL;
	    }

	    // notifications
	    if (!file_exists(storage_path() . "/s3bucket/unprocessedFiles/archive/")) {
	    	mkdir(storage_path() . "/s3bucket/unprocessedFiles/archive/");
		    echo "created " . storage_path(). "/s3bucket/unprocessedFiles/archive/ folder." . PHP_EOL;
	    }


	    // Logs
	    if (!file_exists(storage_path(). "/s3bucket/")) {
		    mkdir(storage_path(). "/s3bucket/");
		    echo "created " . storage_path(). "/s3bucket/ folder." . PHP_EOL;
	    }
	    if (!file_exists(storage_path(). "/s3bucket/unprocessedFiles/")) {
		    mkdir(storage_path(). "/s3bucket/unprocessedFiles/");
		    echo "created " . storage_path(). "/s3bucket/unprocessedFiles/ folder." . PHP_EOL;
	    }

        return 0;
    }
}
