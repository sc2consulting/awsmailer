<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\RecipientEmail;
use App\Models\Sender;
use App\Models\SendingQueue;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;

class sendPlannedMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
//    protected $signature = 'awsMailer:sendPlanned';
    protected $signature = 'awsMailer:sendPlanned {campaign_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
	    Log::info('new batch sending for campaign ' . $this->argument('campaign_id'));

	    $campaign = Campaign::where('id', $this->argument('campaign_id'))->first();
	    if ($campaign === NULL) {
	    	echo "no campaign found with given ID" . PHP_EOL;
	    	return 404;
	    }

	    SendingQueue::removeUnsubscribed();

	    $recipients = SendingQueue::where('campaign_id', $campaign->id)->take(10)->get();
	    if ($recipients->count() === 0) {
		    echo "no recipient found for given id, (no list added?)". PHP_EOL;
		    return 500;
	    }

		Log::info('new batch sending for campaign ' . $this->argument('campaign_id'));

		$subject = $campaign->mail_subject;

	    if (!file_exists(storage_path() . '/campaign/html/' . $campaign->mail_body)) {
		    echo "body file $campaign->mail_body not found in /storage/campaign/html/...";
		    return 404;
	    }
	    $sender = Sender::where('id', $campaign->sender_id)->first();

	    $processList = array();
	    foreach ($recipients as $recipient) {
		    $recipient_record = RecipientEmail::where('email', $recipient->email)->first();
		    if (!$recipient_record->isUsedRecently()) {
			    $processList []= new Process(['php', 'artisan', 'script:sendMail',
				    strval($campaign->id),
				    $subject,
				    $recipient->email,
				    $campaign->mail_body,
				    $campaign->sender_name,
				    $sender->email
			    ]);
				$recipient->delete();
			}
	    }
	    foreach ($processList as $process) {
	    	$process->setTimeout(0);
	    	$process->setIdleTimeout(0);
		    $process->start(function ($type, $buffer) {
			    if (Process::ERR === $type || strpos($buffer, 'ERR:SEND_FAIL;')) {
					Log::info('ERR >> send failed : '.$buffer);
			    } else {
					Log::info('sent email to : ' . $buffer);
			    }
		    });
		    $campaign->email_sent += 1;
	    }
	    $campaign->save();
	    foreach ($processList as $process) {
	    	$process->wait();
		}
		Log::info('finished');
        return 0;
    }
}
