<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\CampaignPlanning;
use App\Models\RecipientEmail;
use App\Models\Sender;
use App\Models\SendingQueue;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;

class ScriptPlanningManager extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Script:PlanningManager';

    /**
     * The console command description.
     *
     * @var string
     */

    protected $description = 'Command description';

	/**
	 * count of mails sent each time the script is runned
	 *
	 * @var int
	 */
	private $sentMailCount;

	/**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->sentMailCount = 0;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//    	Log::info("running planner");
    	SendingQueue::removeUnsubscribed();

    	$plannings = CampaignPlanning::where('active', true)->get();
    	if ($plannings == NULL) {
    		return 0;
	    }

    	$limit = 10;
	    $processList = array();
    	foreach ($plannings as $planning) {
    		if ($planning->active && strtotime($planning->sending_date) < time()) {
			    Log::info( 'checking active planning ' . $planning->id);
    			$campaign = Campaign::where('id', $planning->campaign_id)->first();
    			if ($campaign === NULL) {
    				Log::error("Planning $planning->id has invalid campaign id $planning->campaign_id");
    				continue ;
			    }
    			$sender = Sender::where('id', $campaign->sender_id)->first();
    			if ($sender === NULL) {
    				Log::error("Planning $planning->id has invalid sender id $campaign->sender_id");
    				continue ;
			    }
			    $recipients = SendingQueue::where('campaign_id', $planning->campaign_id)->limit($limit)->get();
			    if (count($recipients) == 0) {
			    	log::info("no recipient left, disabling campaign $campaign->id");
				    $planning->active = false;
				    $planning->save();
				    continue;
			    }
			    Log::info(count($recipients) . " fetched for sending from campaign " . $campaign->id);
	            foreach ($recipients as $recipient) {
		            if (!RecipientEmail::isRecentlyUsed($recipient->email)) {
			            $processList []= new  Process(['php', 'artisan', 'script:sendMail',
				            strval($campaign->id),
				            $campaign->mail_subject,
				            $recipient->email,
				            $campaign->mail_body,
				            $sender->name,
				            $sender->email
			            ]);
			            $r_data = RecipientEmail::where('email', $recipient->email)->first();
			            $r_data->last_sent = date("Y-m-d H:i:s");
			            $r_data->save();
			            $planning->sent_count += 1;
			            $recipient->delete();
		            }
	            }
	            $planning->save();
		    }
	    }
	    foreach ($processList as $process) {
		    $process->setTimeout(0);
		    $process->setIdleTimeout(0);
		    $process->start(function ($type, $buffer) {
			    if (Process::ERR === $type || strpos($buffer, 'ERR:SEND_FAIL;')) {
				    echo 'ERR >> send failed : '.$buffer .PHP_EOL;
				    Log::info('ERR >> send failed : '.$buffer );
			    } else {
				    echo 'sent email to : ' . $buffer . PHP_EOL;
				    Log::info('sent email to : ' . $buffer);
			    }
		    });
	    }
    	foreach ($processList as $process) {
    		$process->wait();
	    }
        return 0;
    }

}
