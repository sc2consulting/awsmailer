<?php

namespace App\Console\Commands;

use App\Models\Sender;
use App\Tools\AwsMailer;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class CreateSender extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sender:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	/*
    	 * get informations from command line
    	 */
    	$email = $this->ask('AWS Email address used: (must be verified on aws ses)');
    	$verify_receiver = $this->ask('Email used to receive sender test:');

		$newSender = new Sender;
    	$newSender->email = $email;
    	$newSender->aws_id = '';
		/*
		 * Test the given information
		 */
	    $mailer = new AwsMailer;
	    $res = $mailer->sendTestMail(
	    	$verify_receiver,
		    'sc2TestMailer',
		    file_get_contents(storage_path() . '/campaign/html/mailSenderVerification.html'),
		    $newSender
	    );
	    if ($res !== true) {
	       	     $newSender->delete();
		    echo 'Error during sender testing' . PHP_EOL;
		    return 0;
	    }
	    echo 'Sender verification success, adding it to database.' . PHP_EOL;

	    /*
	     * add sender to database
	     */
    	$newSender->save();
        return 0;
    }
}
