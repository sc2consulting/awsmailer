<?php

namespace App\Console\Commands;

use App\Models\SentEmail;
use App\Tools\AwsNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class processStorageNotif extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:processStorageFiles';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	$directory = storage_path() . "/s3bucket/unprocessedFiles/";
	    /*
	     * get file list
	     */
	    $fileList = scandir($directory);
	    if ($fileList === false) {
	    	return 0;
	    }

	    $notificationHandler = new AwsNotification();
	    /*
	     *  todo loop file
	     */
	    $count = 0;
	    foreach ($fileList as $filename) {
	    	$fullPath =  $directory . $filename;
		    /*
			 * todo decode content
			 */
//		    echo "reading in " . $directory . $filename . "\n";
		    if (is_dir($fullPath) || $filename === ".DS_Store")
		    	continue ;
		    $rawData = file_get_contents($directory . $filename);

		    $data = json_decode($rawData);
		    /*
			 * todo check if message id exist
			 * todo process content
			 */
		    if ($data !== NULL
			    && property_exists($data, 'notificationType')
			    && SentEmail::getCampaignIdByMessageId($data->mail->messageId) !== NULL) {
		    	var_dump($data);
			    $notificationHandler->processNotification($data);
			    $count++;
			    rename($directory . $filename, $directory . "archive/" . $filename);
		    }
		    else if ($data !== NULL
			    && property_exists($data, 'eventType')
			    && SentEmail::getCampaignIdByMessageId($data->mail->messageId) !== NULL) {
		    	var_dump($data);
			    $notificationHandler->processEvent($data);
			    $count++;
			    rename($directory . $filename, $directory . "archive/" . $filename);
		    }
	    }

	    Log::info("recovered $count notification.");
	    return 0;
    }
}
