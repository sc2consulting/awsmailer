<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\FaiList;
use Illuminate\Console\Command;

class getCampaignFaiStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:getStats {campaign_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		$stats = Campaign::getDetailFaiStats($this->argument('campaign_id'));
		if ($stats == NULL) {
			echo "Campaign::getDetailFaiStats : Returned NULL, no campaign found with given id ?";
			return 0;
		}
		$list = array(['id', 'name', 'delivery', 'open', 'clicks', 'bounce', 'complaint', 'total']);

		foreach ($stats as $key => $value) {
			$fai_name = FaiList::where('id', $key)->first()->name;
			array_push($list, [
				$key,
				$fai_name,
				key_exists('delivery', $value) ? $value['delivery'] : 0,
				key_exists('open', $value) ? $value['open'] : 0,
				key_exists('click', $value) ? $value['click'] : 0,
				key_exists('bounce', $value) ? $value['bounce'] : 0,
				key_exists('complaint', $value) ? $value['complaint'] : 0,
				key_exists('sent', $value) ? $value['sent'] : 0,
			]);
		}

		$mask = "| %5.5s | %-10.10s | %-10.10s | %-10.10s | %-10.10s | %-10.10s | %-10.10s | %-10.10s |\n";
		foreach ($list as $line) {
			printf($mask, $line[0], $line[1], $line[2], $line[3], $line[4], $line[5], $line[6], $line[7]);
		}
        return 0;
    }
}
