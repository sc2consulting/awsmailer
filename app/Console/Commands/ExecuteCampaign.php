<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\FaiList;
use App\Models\RecipientEmail;
use App\Models\Sender;
use App\Models\SendingQueue;
use App\Models\sentEmail;
use App\Tools\AwsMailer;
use Illuminate\Console\Command;

class ExecuteCampaign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:run {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	// Deprecated
	    return 0;
	    $mailer = new AwsMailer;

	    $campaign = Campaign::where('id', $this->argument('id'))->first();
	    if ($campaign === NULL) {
		    echo "no campaign found with given ID". PHP_EOL;
		    return 500;
	    }

	    SendingQueue::removeUnsubscribed();

	    $recipients = SendingQueue::where('campaign_id', $campaign->id)->get();
	    if ($recipients->count() === 0) {
	    	echo "no recipient found for given id, (no list added?)". PHP_EOL;
	    	return 500;
	    }

	    $subject = $campaign->mail_subject;
	    $filepath = storage_path() . '/campaign/html/' . $campaign->mail_body;
	    if (!file_exists($filepath)) {
	    	echo "body file $filepath not found";
	    	return 404;
	    }
	    $body = file_get_contents($filepath);

	    $sender = Sender::where('id', $campaign->sender_id)->first();

	    foreach ($recipients as $recipient) {
		    $email_in_list = RecipientEmail::where('email', $recipient->email)->first();
	    	if (!RecipientEmail::isRecentlyUsed($recipient->email)) {
			    $tmpBody = str_replace('$VARIABLEEMAIL', $recipient->email, $body);
			    $res = $mailer->sendMailSES($campaign->id, $subject, $recipient->email, $tmpBody, $sender);
			    $recipient->delete();

			    if ($res !== false) {
				    $email_in_list->sent_emails += 1;
				    Campaign::update_fai_stat($campaign->id, $recipient->email, 'sent');
			    }
			    else {
				    $email_in_list->sending_failed += 1;
			    }
			    $email_in_list->last_sent = date("Y-m-d H:i:s");
			    $email_in_list->save();
		    }
	    	else {
	    		echo "email " . $email_in_list->email . " has been used recently, skipping." . PHP_EOL;
		    }
	    }
        return 0;
    }
}
