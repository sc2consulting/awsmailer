<?php

namespace App\Console\Commands;

use App\Models\RecipientEmail;
use App\Tools\FileManager;
use Illuminate\Console\Command;

class AwsMailerProcessUnsub extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'awsMailer:processUnsubFile {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	$filename = array_slice(explode("/", $this->argument('filename')), -1)[0];
    	$res = $this->ask("reading in file storage/campaign/unsubList/$filename. ('n' to cancel)");
    	if ($res === 'n') {
    		echo 'import of unsubscribes canceled';
    		return 0;
	    }
	    $list = FileManager::lineToArray("/campaign/unsubList/" . $filename);
    	$count = RecipientEmail::unsubscribeList($list);

    	echo $count . " emails unsubscribed";
        return 0;
    }
}
