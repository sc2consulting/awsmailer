<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\FaiList;
use App\Models\RecipientEmail;
use App\Models\Sender;
use App\Models\SentEmail;
use App\Tools\Encryptor;
use App\Tools\Env;
use Aws\Ses\Exception\SesException;
use Aws\Ses\SesClient;
use Html2Text\Html2Text;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;

class SendTestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:sendTestEmail {campaign_id} {recipient}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

	/**
	 * @var array
	 */
	private $key;

	/**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
	    $this->key = Env::getEnvFromFile('aws.key');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
	    $email = $this->argument('recipient');
    	$campaign = Campaign::where('id', intval($this->argument('campaign_id')))->first();
		$sender_email = 'sylvain@sc2consulting.fr';

//    	strval($campaign->id);
		$subject = $campaign->mail_subject;
//		$sender_email = $recipient->email;
		$htmlBody = file_get_contents(storage_path() . "/campaign/html/" . $campaign->mail_body);
		$sender_name = $campaign->sender_name;

	    $htmlBody = str_replace('$VARIABLEEMAIL', $email, $htmlBody);
	    $htmlBody = str_replace('$UNSUBEMAILLINK', 'https://e.desabaz.com/news/unsub?token=' . Encryptor::simple_encrypt($email), $htmlBody);

	    $sesClient = new SesClient([
		    'credentials' => [
			    'key'    => $this->key['AWS_S3_READER_KEY_ID'],
			    'secret' => $this->key['AWS_S3_READER_SECRET']
		    ],
		    'region' => 'us-west-2',
		    'version' => 'latest'
	    ]);

	    $html2txt = new Html2Text($htmlBody);

	    $param = [
		    'ConfigurationSetName' => 'openClickNotif',
		    'Destination' => [ // REQUIRED
			    'ToAddresses' => [$email],
		    ],
		    'Message' => [ // REQUIRED
			    'Body' => [ // REQUIRED
				    'Html' => [
					    'Charset' => 'utf-8',
					    'Data' => $htmlBody, // REQUIRED
				    ],
				    'Text' => [
					    'Charset' => 'utf-8',
					    'Data' => ($html2txt->getText()), // REQUIRED
				    ],
			    ],
			    'Subject' => [ // REQUIRED
				    'Charset' => 'utf-8',
				    'Data' => $subject, // REQUIRED
			    ],
		    ],
		    'Source' => $sender_name . " <" . $sender_email . ">", // REQUIRED
	    ];
	    try {
		    $sesClient->sendEmail($param);
		    echo 'sent test email to :' . $email . PHP_EOL;
		    return true;
	    } catch (SesException $e) {
		    echo 'ERR:SEND_FAIL;' . $email . ';' . $e->getAwsErrorMessage() . PHP_EOL;
		    return false;
	    }
    }
}
