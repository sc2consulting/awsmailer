<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\FaiList;
use App\Models\RecipientEmail;
use App\Models\SentEmail;
use App\Tools\Encryptor;
use App\Tools\Env;
use App\Tools\FileManager;
use Aws\Ses\Exception\SesException;
use Aws\Ses\SesClient;
use Html2Text\Html2Text;
use Illuminate\Console\Command;

class ScriptSendSingleEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'script:sendMail {campaign_id} {subject} {email} {htmlPath} {senderName} {senderEmail}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

	/**
	 * contains aws keys
	 *
	 * @var array
	 */
	private $key;

	/**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
	    $this->key = Env::getEnvFromFile('aws.key');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	$htmlBody = file_get_contents(storage_path() . "/app/campaign/html/" . $this->argument('htmlPath'));
    	$email = $this->argument('email');
    	$campaign_id = intval($this->argument('campaign_id'));
    	$subject = $this->argument('subject');
    	$senderName = $this->argument('senderName');
    	$senderEmail = $this->argument('senderEmail');

	    $htmlBody = str_replace('$VARIABLEEMAIL', $email, $htmlBody);
	    $htmlBody = str_replace('$UNSUBEMAILLINK', 'https://e.desabaz.com/news/unsub?token=' . Encryptor::simple_encrypt($email), $htmlBody);

	    $sesClient = new SesClient([
		    'credentials' => [
			    'key'    => $this->key['AWS_S3_READER_KEY_ID'],
			    'secret' => $this->key['AWS_S3_READER_SECRET']
		    ],
		    'region' => 'us-west-2',
		    'version' => 'latest'
	    ]);

	    $html2txt = new Html2Text($htmlBody);

	    $param = [
		    'ConfigurationSetName' => 'openClickNotif',
		    'Destination' => [ // REQUIRED
			    'ToAddresses' => [$email],
		    ],
		    'Message' => [ // REQUIRED
			    'Body' => [ // REQUIRED
				    'Html' => [
					    'Charset' => 'utf-8',
					    'Data' => $htmlBody, // REQUIRED
				    ],
				    'Text' => [
					    'Charset' => 'utf-8',
					    'Data' => ($html2txt->getText()), // REQUIRED
				    ],
			    ],
			    'Subject' => [ // REQUIRED
				    'Charset' => 'utf-8',
				    'Data' => $subject, // REQUIRED
			    ],
		    ],
		    'Source' => $senderName . " <" . $senderEmail . ">", // REQUIRED
//			'SourceArn' => '',
//			'Tags' => [
//				[
//					'Name' => '<string>', // REQUIRED
//					'Value' => '<string>', // REQUIRED
//				],
		    // ...
//			],
	    ];
	    try {
		    $res = $sesClient->sendEmail($param);

		    $recipient_email = RecipientEmail::where('email', $email)->first();
		    $recipient_email->sent_emails += 1;
		    $recipient_email->last_sent = date("Y-m-d H:i:s");
		    $recipient_email->save();

		    Campaign::update_fai_stat(intval($campaign_id), $email, 'sent');

		    $campaign = Campaign::where('id', $campaign_id)->first();
		    $campaign->email_sent += 1;
		    $campaign->save();

		    $newMail = new SentEmail;
		    $newMail->email = $email;
		    $newMail->campaign_id = $campaign_id;
		    $newMail->aws_message_id = $res->get('MessageId');
		    $newMail->fai_id = FaiList::getFaiId($email);
		    $newMail->save();
		    echo $email;
		    return true;
	    } catch (SesException $e) {
		    $recipient_email = RecipientEmail::where('email', $email)->first();
		    $recipient_email->sending_failed += 1;
		    $recipient_email->save();

		    echo 'ERR:SEND_FAIL;' . $email . ';' . $e->getAwsErrorMessage();
		    return false;
	    }
    }
}
