<?php

namespace App\Console\Commands;

use App\Models\SendingQueue;
use Illuminate\Console\Command;

class QueueRemove extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:removeQueue {campaign_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	$list = SendingQueue::where('campaign_id', $this->argument('campaign_id'))->get();
    	foreach ($list as $element) {
    		$element->delete();
	    }
        return 0;
    }
}
