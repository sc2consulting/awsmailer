<?php

namespace App\Console\Commands;

use App\Tools\Env;
use App\Tools\FileManager;
use Aws\S3\S3Client;
use Illuminate\Console\Command;

class checkBucketContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 's3bucket:showContent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

	private $key;

	/**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->key = Env::getEnvFromFile('aws.key');
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
	    $s3client = S3Client::factory(array(
		    'credentials' => [
			    'key'    => $this->key['AWS_S3_READER_KEY_ID'],
			    'secret' => $this->key['AWS_S3_READER_SECRET']
		    ],
//			'AWS_ACCESS_KEY_ID' => $this->key['AWS_ACCESS_KEY_ID'],
//			'AWS_SECRET_ACCESS_KEY' => $this->key['AWS_SECRET_ACCESS_KEY'],
		    'region' => 'us-west-2',
		    'version' => 'latest'
	    ));

	    /*
		 * Get all Object Names in Bucket
		 */
	    $objectList = $s3client->listObjectsV2([
		    'Bucket' => $this->key['AWS_S3_BUCKET_NAME'],
		    'prefix' => 'ses-notif-'
	    ]);
	    $objectList = $objectList->get('Contents');

	    foreach ($objectList as $item) {
		    if (strpos($item['Key'], "ses_notif") !== false) {
			    $result = $s3client->getObject([
				    'Bucket' => $this->key['AWS_S3_BUCKET_NAME'],
				    'Key' => $item['Key']
			    ]);
			    $data = $result["Body"];
			    $data = (json_decode($data));
			    print ("===========");
//			    var_dump($data);
			    if (property_exists($data, 'notificationType')) {
				    var_dump($data->notificationType);
				    var_dump($data->mail->messageId);
			    }
			    else if (property_exists($data, 'eventType')) {
				    var_dump($data->eventType);
				    var_dump($data->mail->messageId);

			    }
//			    $filename = array_slice((explode('/', $item['Key'])), -1)[0];
//			    $path = "s3bucket/unprocessedFiles/";
//			    FileManager::write_in_storage($path, $filename, $result['Body']);
		    }
	    }

	    return 0;
    }
}
