<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\Sender;
use Illuminate\Console\Command;
use SebastianBergmann\CodeCoverage\Report\PHP;

class CreateCampaign extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'campaign:create';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 */
	public function handle()
	{
		/*
		 * display name of campaign
		 */
		$name = $this->ask('campaign name (cannot be only numbers)');

		$subject = $this->ask('subject of the mail');;

	  //  $subject = $this->ask('subject of the mail');;
//	    while ($subject === '' || !ctype_alnum(str_replace(' ', '', $subject))) {
//		    $subject = $this->ask('subject of the mail');
//	    }

		/*
		 * sender name
		 */
		$sender_display_name = $this->ask('display name on the mail');

		/*
		 * sender email
		 */
		foreach (Sender::all() as $sender) {
			echo $sender['id'] . " : " . $sender['name'] . " ==> " . $sender['email'] . PHP_EOL;
		}
		$sender = -1;
		while (Sender::where('id', $sender)->first() === NULL) {
			$sender = $this->ask('sender id');
		}

		/*
		 * html body
		 */
		$htmlList = scandir(storage_path() . "/campaign/html");
		foreach ($htmlList as $filename) {
			if ($filename != '.' && $filename != '..') {
				echo "> " . $filename . PHP_EOL;
			}
		}
		$htmlFile = $this->ask('file containing HTML (copy paste it from list above)');

		$sender_email = Sender::where('id', $sender)->first()->email;
		echo "Campaign '$name' sending as '$sender_display_name <$sender_email>'" . PHP_EOL .
			"Subject : $subject. Body : $htmlFile" . PHP_EOL;
		if ($this->ask('confirm information? (anything other than "yes" will abort)') === "yes") {
			echo 'creating campaign' . PHP_EOL;
		}
		else {
			echo 'Aborted.' . PHP_EOL;
			return 0;
		}

		/*
		$name = "Campagne TEST 2";
        $subject = "Campagne TEST OBJECT 2";
        $sender_display_name ="Campagne TEST 2";
        $sender = 1;
        $htmlFile ="test.html";

		*/

	    $newCampaign = new Campaign;
		$newCampaign->name = $name;
		$newCampaign->sender_id = $sender;
		$newCampaign->sender_name = $sender_display_name;
		$newCampaign->mail_subject = $subject;
		$newCampaign->mail_body = $htmlFile;
		$newCampaign->save();
		echo 'Campaign created. with ID ' . $newCampaign->id . PHP_EOL;
		return 0;
	}
}
