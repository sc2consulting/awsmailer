<?php

namespace App\Console\Commands;

use App\Models\campaign;
use App\Models\SentEmail;
use App\Models\SESNotification;
use App\Tools\AwsNotification;
use App\Tools\Env;
use App\Tools\FileManager;
use Aws\S3\S3Client;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AwsSESNotification extends Command
{
	/**
	 * Contains Key and raw variables to AWS services
	 *
	 * @var array
	 */
	private $key;

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'aws:getSESNotification';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description =
		'Fetch Mails present in AWS S3 Bucket and Sort
		them into the database according to their status.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->key = Env::getEnvFromFile('aws.key');
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return int
	 * @throws Exception
	 */
	public function handle()
	{
		Log::info('fetching notifications');
		$s3client = S3Client::factory(array(
			'credentials' => [
				'key'	=> $this->key['AWS_S3_READER_KEY_ID'],
				'secret' => $this->key['AWS_S3_READER_SECRET']
			],
			'region' => 'us-west-2',
			'version' => 'latest'
		));

		/*
		 * Get all Object Names in Bucket
		 */
		$objectList = $s3client->listObjectsV2([
			'Bucket' => $this->key['AWS_S3_BUCKET_NAME'],
			'prefix' => 'ses-notif-'
		]);
		$objectList = $objectList->get('Contents');

		$notificationHandler = new AwsNotification();

		/*
		 * Get results
		 */
		$count = 0;
		$errors = 0;
		foreach ($objectList as $item) {
			if (strpos($item['Key'], "ses_notif") !== false) {
				$result = $s3client->getObject([
					'Bucket' => $this->key['AWS_S3_BUCKET_NAME'],
					'Key' => $item['Key']
				]);
				$count++;
				$data = $result["Body"];
				$data = (json_decode($data));

				/*
				 * Process fetched S3 data
				 */
				if ($data !== NULL
					&& property_exists($data, 'notificationType')
					&& SentEmail::getCampaignIdByMessageId($data->mail->messageId) !== NULL) {
					$notificationHandler->processNotification($data);
				}
				else if ($data !== NULL
					&& property_exists($data, 'eventType')
					&& SentEmail::getCampaignIdByMessageId($data->mail->messageId) !== NULL) {
					$notificationHandler->processEvent($data);
				}
				else {
					Log::info('unregistered message id or wrong file format / unknown filetype.');
					echo "wrong file format or unknown filetype.";
					$filename = array_slice((explode('/', $item['Key'])), -1)[0];
					$path = "s3bucket/unprocessedFiles/";
					FileManager::write_in_storage($path, $filename, $result['Body']);
					echo ' data saved in ' . $path . PHP_EOL;
					$errors++;
				}

				//remove processed file from S3
				$s3client->deleteObject([
					'Bucket' => $this->key['AWS_S3_BUCKET_NAME'],
					'Key' => $item['Key']
				]);

			}
		}
		Log::info("processed $count files");
		echo "processed $count files" . PHP_EOL;
		if ($errors > 0) {
			Log::info("including $errors. stored in /storage/s3bucket/unprocessedFiles/");
			echo "including $errors. stored in /storage/s3bucket/unprocessedFiles/" . PHP_EOL;
		}
		Log::info('notification fetching complete');
		return 0;
	}
}
