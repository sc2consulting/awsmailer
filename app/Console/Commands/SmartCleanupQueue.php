<?php

namespace App\Console\Commands;

use App\Models\RecipientEmail;
use App\Models\SendingQueue;
use Illuminate\Console\Command;

class SmartCleanupQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:SmartClearQueue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'removes any email in sending queue that has a issue (bounce / complaint / unsub)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	$queued_emails = SendingQueue::all();
    	foreach ($queued_emails as $email_in_queue) {
    		$info = RecipientEmail::where('email', $email_in_queue->email)->first();
    		if ($info->complaints > 0 || $info->bounces > 0 || $info->unsubscribe != 0) {
    			$email_in_queue->delete();
		    }
	    }
        return 0;
    }
}
