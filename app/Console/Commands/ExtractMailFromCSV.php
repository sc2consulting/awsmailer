<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExtractMailFromCSV extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csv:exportMail {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	$filename = $this->argument('filename');
    	if (!is_file($filename)) {
    		echo "Given file doesn't exists or is not a file". PHP_EOL;
    		return -1;
	    }
    	$fh = fopen($filename, 'r');
    	$headers = explode(';', fgets($fh));
    	for ($i = 0; $i < count($headers); $i++) {
    		echo $i . " : " . $headers[$i] . PHP_EOL;
	    }
    	$index = -1;
    	while ($index < 0 || $index >= count($headers)) {
		    $index = intval($this->ask("enter index of column containing emails"));
	    }
	    file_put_contents($filename . '.out', "");
	    while ($line = fgets($fh)) {
    		$email = trim((explode(';', $line))[$index]);
		    file_put_contents($filename . '.out', $email. PHP_EOL, FILE_APPEND);
	    }
	    echo "saved in ". $filename. ".out";
        return 0;
    }
}
