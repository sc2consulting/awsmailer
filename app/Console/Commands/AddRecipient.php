<?php

namespace App\Console\Commands;

use App\Models\Campaign;
use App\Models\FaiList;
use App\Models\RecipientEmail;
use App\Models\SendingQueue;
use App\Tools\MailTools;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AddRecipient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaign:addRecipient {campaign_id} {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
    	$filepath = storage_path() . "/campaign/mailList/" . $this->argument('filename');
    	if (Campaign::where('id', $this->argument('campaign_id'))->first() === NULL) {
    		echo 'Campaign with given id does not exists';
    		return 404;
	    }
    	$campaign_id = $this->argument('campaign_id');

    	if (!file_exists($filepath)) {
    		echo "specified $filepath not found in /campaign/mailList/" . PHP_EOL;
    		return 404;
	    }
    	$fh = fopen($filepath, 'r');
    	$count = 0;
    	$added = 0;
		while ($line = fgets($fh)) {
			$email = trim(trim($line, ';'));
			if (MailTools::isEmail($email)) {

				if ($this->isQueueable($email, $campaign_id)) {
					$newRecipient = new SendingQueue;
					$newRecipient->email = $email;
					$newRecipient->fai_id = FaiList::getFaiId($email);
					$newRecipient->campaign_id = $campaign_id;
					$newRecipient->save();
					$added++;
				}
			}
			else {
				echo $email . " is not an email at L" . $count . PHP_EOL;
			}
			$count++;
		}
		echo "added $added mails to campaign #" . $this->argument('campaign_id') . PHP_EOL;
        return 0;
    }

	public function isQueueable(string $email, int $campaign_id)
	{
//		RecipientEmail::isRecentlyUsed($email);

//		Log::info('queueable ? for' . $email);

		/*
		 * verify is this mail has been used previously
		 */
		$email_db = RecipientEmail::where('email', $email)->first();
		if ($email_db === NULL) {
			$newEntry = new RecipientEmail;
			$newEntry->email = $email;
			$newEntry->fai_id = FaiList::getFaiId($email);
			$newEntry->save();
			return true;
		}

		// check unsubscribe, bounces and complaints
		if ($email_db->unsubscribe != 1) {
			echo "$email is unsubscribed." . PHP_EOL;
			return false;
		}
		if ($email_db->bounces > 0 || $email_db->complaints > 0) {
			echo "$email bounced / complained / failed previously" . PHP_EOL;
			return false;
		}

		/*
		 * verify if mail is currently in queue for the same campaign
        */
		foreach (SendingQueue::where('email', $email)->cursor() as $mail_in_queue) {
			if ($mail_in_queue->campaign_id == $campaign_id) {
				echo "$email is already queue for this campaign" . PHP_EOL;
				return false;
			}
		}
//		if ($records = SendingQueue::where('email', $email)->get() != NULL) {
//			foreach ($records as $mail_in_queue) {
//			}
////			Log::info($rec->email . " is in queue");
////			return false;
//		}

		// verify if mail is currently in queue
//		if (SendingQueue::where('email', $email)->first() !== NULL) {
//			echo "$email is already queued for a campaign" . PHP_EOL;
//			return false;
//		}

		// verify if an email has been sent recently to this address
//		if (RecipientEmail::isRecentlyUsed($email)) {
//			echo "$email has recently recieved a mail" . PHP_EOL;
//			return false;
//		}
		return true;
	}

}
