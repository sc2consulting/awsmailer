<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, $message_id)
 */
class SESNotification extends Model
{
	use HasFactory;

	protected $table = 'ses_notification';

	public static function addNewNotification($campaign_id, $email, $timestamp, $data, $arrivalDate = '')
	{
		/*
		 * basic information
		 */
		$notificationType = $data->notificationType;
		$faiId = FaiList::getFaiId($email);

		$newNotification = new SESNotification;
		$newNotification->message_id = $data->mail->messageId;
		$newNotification->email = $email;
		$newNotification->campaign_id = $campaign_id;
		$newNotification->notified_at = $timestamp;
		$newNotification->status = $notificationType;
		$newNotification->fai_id = $faiId;

		/*
		 * specific informations
		 */
		switch ($notificationType) {
			case 'Delivery':
				$newNotification->smtp_response = $data->delivery->smtpResponse;
				break;
			case 'Bounce':
				$newNotification->status_type = $data->bounce->bounceType;
				$newNotification->status_sub_type = $data->bounce->bounceSubType;
				break;
			case 'Complaint':
//				file_put_contents(storage_path() . "/logs/debug.json", json_encode($data, JSON_PRETTY_PRINT));

				$newNotification->arrivalDate = $arrivalDate;
				$newNotification->status_type = isset($data->complaint->complaintFeedbackType) ? $data->complaint->complaintFeedbackType : "";
				//$newNotification->status_type = $data->complaint->complaintFeedbackType;
				$newNotification->status_sub_type = $data->complaint->complaintSubType;
				break;
		}

		/*
		 * save the rest
		 */
		$newNotification->others = json_encode($data);
		$newNotification->save();
	}

	public static function getCampaignIdByMessageId($message_id)
	{
		$result = SESNotification::where('message_id', $message_id)->first();
		if ($result !== NULL) {
			return $result->attributes['campaign_id'];
		}
		return NULL;
	}
}
