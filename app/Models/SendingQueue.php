<?php

namespace App\Models;

use App\Tools\MailTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $key, $value)
 * @method static insert(array $emails)
 * @method static whereIn(string $string, array $emails)
 */
class SendingQueue extends Model
{
    use HasFactory;
    protected $table = 'sending_queue';

	public static function removeUnsubscribed()
	{
		// todo : to change for performance
		$all = SendingQueue::all();
		foreach ($all as $email) {
			$recipient = RecipientEmail::where('email', $email->email)->first();
			if ($recipient->unsubscribe === 1
				&& $recipient->bounces > 0
				&& $recipient->complaints > 0) {
				$email->delete();
			}
	    }
	}

	public static function add($email, $campaign_id)
	{
		if (!MailTools::isEmail($email))
			return "not_a_mail";
		if (Campaign::where('id', $campaign_id)->first() === NULL)
			return 'campaign_unknown';

		if (($recipient = RecipientEmail::where('email', $email)->first()) === NULL) {
			$recipient = new RecipientEmail();
			$recipient->email = $email;
			$recipient->fai_id = FaiList::getFaiId($email);
			$recipient->save();
		}
		if ($recipient->complaints > 0) {
			return 'complaint';
		}
		if ($recipient->bounces > 0) {
			return 'bounce';
		}
		if ($recipient->unsubscribe != 0) {
			return 'unsubscribe';
		}
		if (SendingQueue::where('email', $email)->where('campaign_id', $campaign_id)->first() !== NULL) {
			return 'in_queue';
		}
		$newEntry = new SendingQueue();
		$newEntry->email = $recipient->email;
		$newEntry->fai_id = $recipient->fai_id;
		$newEntry->campaign_id = $campaign_id;
		$newEntry->save();
		return "added";
	}

	public static function addFileList($filename, $campaign_id)
	{
		$add_info = ['complaint' => 0, 'bounce' => 0, 'unsubscribe' => 0, 'in_queue' => 0, 'added' => 0];
		$emails = array();

		$fh = fopen(storage_path() . '/app/campaign/mailList/' . $filename, 'r');
		$count = 0;
		while ($line = fgets($fh)) {
			if (($tmpVar = trim(trim($line, ';'))) && MailTools::isEmail($tmpVar)) {
				if (!in_array($tmpVar, $emails)) {
					array_push($emails, $tmpVar);
					$count++;
				}
			}
			if ($count == 2000) {
				self::TryAddList($emails, $add_info, $campaign_id);
				$emails = array();
				$count = 0;
//				return $add_info;
			}
		}
		self::TryAddList($emails, $add_info, $campaign_id);
		return $add_info;
	}

	private static function tryAddList($emails, &$resultTable, $campaign_id)
	{
		$finalList = array();
		/*
		 * remove exist in queue
		 */

//		$currentQueue = SendingQueue::where('campaign_id', intval($campaign_id))->get();
		$existingInQueue = SendingQueue::where('campaign_id', $campaign_id)->whereIn('email', $emails)->get();
		foreach ($existingInQueue as $elem) {
			unset($emails[array_search($elem->email, $emails)]);
			$resultTable['in_queue'] += 1;
		}

		$existingData = RecipientEmail::whereIn('email', $emails)->get();

		foreach ($existingData as $elem) {
//			echo "$elem->email <br>";
			if ($elem->complaints > 0)
				$resultTable['complaint'] += 1;
			elseif ($elem->bounces > 0)
				$resultTable['bounce'] += 1;
			elseif ($elem->unsubscribe)
				$resultTable['unsubscribe'] += 1;
//			elseif ($currentQueue->where('email', $elem->email)->first())
//				$resultTable['in_queue'] += 1;
			else
				array_push($finalList, $elem->email);
			unset($emails[array_search($elem->email, $emails)]);
		}

		foreach ($emails as $email) {
			$recipient = new RecipientEmail();
			$recipient->email = $email;
			$recipient->fai_id = FaiList::getFaiId($email);
			$recipient->save();
			array_push($finalList, $email);
			unset($emails[array_search($email, $emails)]);
		}

		$resultTable['added'] += count($finalList);
		$data = array();
		foreach ($finalList as $email) {
			$data []= [
				'email' => $email,
				'fai_id' => FaiList::getFaiId($email),
				'campaign_id' => $campaign_id,
			    'created_at' => now(),
                'updated_at' => now()
			];
		}
		SendingQueue::insert($data);
	}

//	public static function addFileList($filename, $campaign_id)
//	{
//		$fh = fopen(storage_path() . $filename, 'r');
//
//		$add_info = ['complaint' => 0, 'bounce' => 0, 'unsubscribe' => 0, 'in_queue' => 0, 'added' => 0];
//		while ($line = fgets($fh)) {
//			$email = trim(trim($line, ';'));
//			$addRes = self::add($email, $campaign_id);
//			if (!key_exists($addRes, $add_info))
//				$add_info[$addRes] = 0;
//			$add_info[$addRes] += 1;
//		}
//		return $add_info;
//	}

	public static function countByCampaignID($campaign_id)
	{
		return SendingQueue::where('campaign_id', $campaign_id)->get()->count();
	}
}