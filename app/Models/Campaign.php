<?php

namespace App\Models;

use App\Tools\StringTool;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $key, $value)
 * @method static orderBy(string $key, string $argument)
 *
 * @property String name
 * @property int sender_id
 * @property String sender_name
 * @property String mail_subject
 * @property String mail_body
 */
class Campaign extends Model
{
    use HasFactory;

    protected $table = 'campaign';

    public static function getCampaignEmail($id)
    {
	    $campaign = Campaign::where('id', $id)->first()->attributes;
	    return Sender::getEmailById($campaign['sender_id']);
    }

    public static function getDetailFaiStats($id)
    {
    	if (($campaign = Campaign::where('id', $id)->first()) != NULL) {
		    return json_decode($campaign->attributes['fai_stats'], true);
	    }
    	else {
		    return NULL;
	    }
    }

    public static function getIdByName($name)
    {
    	$campaign = Campaign::where('name', $name)->first();
    	return $campaign->attribute['id'];
    }

	/**
	 * @param $campaign_id Int
	 * @param $email String
	 * @param $resType String[delivery, complaint, bounce]
	 */
	public static function update_fai_stat(int $campaign_id, string $email, string $resType)
    {
	    $campaign = Campaign::where('id', $campaign_id)->first();
	    $fai_stats = array();
	    if ($campaign->fai_stats !== NULL) {
		    $fai_stats = json_decode($campaign->fai_stats, true);
	    }
	    $fai_id = FaiList::getFaiId($email);
	    if (!key_exists($fai_id, $fai_stats)) {
	    	$fai_stats[$fai_id] = [];
	    }
	    if (!key_exists($resType, $fai_stats[$fai_id])) {
		    $fai_stats[$fai_id][$resType] = 0;
	    }
		$fai_stats[$fai_id][$resType] += 1;
		$json_fai_stats = json_encode($fai_stats);
	    $campaign->fai_stats = $json_fai_stats;
	    $campaign->save();
    }

    public function deleteWithQueue()
    {
    	SendingQueue::where('campaign_id', $this->id)->delete();
    	return $this->delete();
    }
}
