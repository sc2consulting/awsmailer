<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $key, $value)
 */
class SentEmail extends Model
{
    use HasFactory;

    protected $table = 'sent_email';

	public static function getCampaignIdByMessageId($message_id)
	{
		$data = SentEmail::where('aws_message_id', $message_id)->first();
		if ($data != NULL) {
			return $data->campaign_id;
		}
		return NULL;
	}
}
