<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $key, $value)
 * @method static whereDate(string $key, $comparator, $value)
 * @property string campaignName
 * @property string campaign_id
 * @property string sending_date
 * @property boolean active
 */
class CampaignPlanning extends Model
{
	use HasFactory;
	protected $table = 'campaign_planning';
}
