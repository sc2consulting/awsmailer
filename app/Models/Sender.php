<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @method static where(string $key, $value)
 */
class Sender extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'sender';

    public static function getEmailById($id)
    {
	    $sender = Sender::where('id', $id)->first()->attributes;
	    return $sender['email'];
    }

    public static function getIdByName($name)
    {
    	$sender = Sender::where('name', $name)->first()->attributes;
		return $sender['id'];
    }

    public static function getIdByEmail($email)
    {
	    $sender = Sender::where('email', $email)->first()->attributes;
	    return $sender['id'];
    }

	public static function getSender($sender_id)
	{
		return Sender::where('id', $sender_id)->first()->attributes;
	}

	public static function getSenderList()
	{
		return Sender::all();
	}
}
