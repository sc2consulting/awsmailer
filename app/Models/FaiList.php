<?php

namespace App\Models;

use App\Tools\MailTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 * @method static where(string $key, $value, $value = null)
// * @method static where(string $key, $value)
 */
class FaiList extends Model
{
    use HasFactory;

    protected $table = 'fai_list';

	public static function getFaiId($email)
	{
		$fai = MailTools::getFai($email);
		$result = FaiList::where('ndd', 'LIKE', "%".$fai."%")->first();
		if ($result === NULL) {
			if (strpos($fai, '.fr') >= strlen($fai) - 3) {
				return 12;
			}
			return 7;
		}
		return $result->attributes['id'];
	}


}
