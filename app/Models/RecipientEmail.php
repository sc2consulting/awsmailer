<?php

namespace App\Models;

use App\Tools\MailTools;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $key, $value)
 * @method static whereIn(string $string, array $emails)
 */
class RecipientEmail extends Model
{
    use HasFactory;

	//const recent_in_seconds = 2*24*60*60;
	const recent_in_seconds = 60 * 60;
//    const recent_in_seconds = 2*60;
    public static function isRecentlyUsed($email)
    {
    	$elem = RecipientEmail::where('email', $email)->first();
    	if ($elem === NULL || $elem->last_sent === NULL) {
    		return false;
	    }
    	if (strtotime($elem->last_sent) < time() - self::recent_in_seconds) {
		    return false;
	    }
	    return true;
    }

    public function isUsedRecently()
    {
    	if ($this->last_sent === NULL) {
    		return false;
	    }
    	if (strtotime($this->last_sent) < time() - self::recent_in_seconds) {
		    return false;
	    }
	    return true;
    }

	public static function unsubscribeList($list)
	{
		$count = 0;
		foreach ($list as $email) {
			$email = trim(trim($email), ";");
			if (!MailTools::isEmail($email)) {
				echo "$email is not an email". PHP_EOL;
				continue;
			}
			$recipient = RecipientEmail::where('email', $email)->first();
			if ($recipient === NULL) {
				$recipient = new RecipientEmail;
				$recipient->email = $email;
				$recipient->fai_id = FaiList::getFaiId($email);
				echo "unlisted recipient $email found in list. Created an entry.". PHP_EOL;
			}
			if ($recipient->unsubscribe != true) {
				$recipient->unsubscribe = true;
				$count++;
			}
			$recipient->save();
		}
		return $count;
	}
}
