<?php


namespace App\Tools;


use App\Models\Campaign;
use App\Models\FaiList;
use App\Models\RecipientEmail;
use App\Models\sentEmail;
use App\Models\SESNotification;

class AwsNotification
{
	public function processNotification($data) {
		$responseType = $data->notificationType;
		$message_id = $data->mail->messageId;
		$campaign_id = sentEmail::getCampaignIdByMessageId($message_id);

		/*
		 * process result with related previous data
		 */
		if (method_exists($this, 'process'. $responseType . 'Result')) {
			$method = 'process' . $responseType . 'Result';
			$this->$method($data, $campaign_id, false);
		}
		else {
			echo 'method AwsSESNotification->process'. $responseType . 'Result() not found' . PHP_EOL;
			echo 'file was most likely not a notification from SES';
			$path = "notificationErrorLog/";
			$filename = $message_id . ".json";
			FileManager::write_in_storage($path, $filename, json_encode($data, JSON_PRETTY_PRINT));
			echo 'data saved in ' . $path . PHP_EOL;
		}

	}

	public function processEvent($data) {
		$responseType = $data->eventType;
		$message_id = $data->mail->messageId;
		$campaign_id = sentEmail::getCampaignIdByMessageId($message_id);

		/*
		 * process result with related previous data
		 */
		if (method_exists($this, 'process'. $responseType . 'Result')) {
			$method = 'process' . $responseType . 'Result';
			$this->$method($data, $campaign_id, false);
		}
		else {
			echo 'method AwsSESNotification->process'. $responseType . 'Result() not found' . PHP_EOL;
			echo 'file was most likely not an event from SES';
			$path = "notificationErrorLog/";
			$filename = $message_id . ".json";
			FileManager::write_in_storage($path, $filename, json_encode($data, JSON_PRETTY_PRINT));
			echo 'data saved in ' . $path . PHP_EOL;
		}

	}

	private function processDeliveryResult($data, $campaign_id)
	{
		$email = $data->delivery->recipients[0];
		$timestamp = str_replace(['T', 'Z'], [' ', ''], $data->delivery->timestamp);
		SESNotification::addNewNotification($campaign_id, $email, $timestamp, $data);

		$campaign = Campaign::where('id', $campaign_id)->first();
		$campaign->delivery += 1;
		$campaign->save();

		$recipient = RecipientEmail::where('email', $email)->first();
		$recipient->delivered += 1;
		$recipient->save();

		Campaign::update_fai_stat($campaign_id, $email, 'delivery');
	}

	private function processBounceResult($data, $campaign_id)
	{
		$email = $data->bounce->bouncedRecipients[0]->emailAddress;
		$timestamp = str_replace(['T', 'Z'], [' ', ''], $data->bounce->timestamp);
		SESNotification::addNewNotification($campaign_id, $email, $timestamp, $data);

		$campaign = Campaign::where('id', $campaign_id)->first();
		$campaign->bounces += 1;
		$campaign->save();

		$recipient = RecipientEmail::where('email', $email)->first();
		if ($recipient === NULL) {
			$recipient = new RecipientEmail;
			$recipient->email = $email;
			$recipient->fai_id = FaiList::getFaiId($email);
			$recipient->sent_emails += 1;
			$recipient->last_sent = date("Y-m-d H:i:s");
		}
		$recipient->bounces += 1;
		$recipient->save();

		Campaign::update_fai_stat($campaign_id, $email, 'bounce');
	}

	private function processComplaintResult($data, $campaign_id)
	{
		$email = $data->complaint->complainedRecipients[0]->emailAddress;
		$timestamp = str_replace(['T', 'Z'], [' ', ''], $data->complaint->timestamp);
		$arrivalDate = str_replace(['T', 'Z'], [' ', ''], $data->complaint->arrivalDate);
		SESNotification::addNewNotification($campaign_id, $email, $timestamp, $data, $arrivalDate);


		$campaign = Campaign::where('id', $campaign_id)->first();
		$campaign->complaints += 1;
		$campaign->save();

		$recipient = RecipientEmail::where('email', $email)->first();
		$recipient->complaints += 1;
		$recipient->save();

		Campaign::update_fai_stat($campaign_id, $email, 'complaint');
	}

	private function processOpenResult($data, $campaign_id)
	{
		$email = $data->mail->destination[0];

		$ses_notif = new SESNotification();
		$ses_notif->email = $email;
		$ses_notif->message_id = $data->mail->messageId;
		$ses_notif->campaign_id = $campaign_id;
		$ses_notif->notified_at = str_replace(['T', 'Z'], [' ', ''], $data->mail->timestamp);
		$ses_notif->status = $data->eventType;
		$ses_notif->fai_id = FaiList::getFaiId($email);
		$ses_notif->save();

		$campaign = Campaign::where('id', $campaign_id)->first();
		$campaign->open += 1;
		$campaign->save();

		$recipient = RecipientEmail::where('email', $email)->first();
		$recipient->open += 1;
		$recipient->save();

		Campaign::update_fai_stat($campaign_id, $email, 'open');
	}

	private function processClickResult($data, $campaign_id)
	{
		$email = $data->mail->destination[0];

		$ses_notif = new SESNotification();
		$ses_notif->email = $email;
		$ses_notif->message_id = $data->mail->messageId;
		$ses_notif->campaign_id = $campaign_id;
		$ses_notif->notified_at = str_replace(['T', 'Z'], [' ', ''], $data->mail->timestamp);
		$ses_notif->status = $data->eventType;
		$ses_notif->fai_id = FaiList::getFaiId($email);
		$ses_notif->save();

		$campaign = Campaign::where('id', $campaign_id)->first();
		$campaign->click += 1;
		$campaign->save();

		$recipient = RecipientEmail::where('email', $email)->first();
		$recipient->click += 1;
		$recipient->save();

		Campaign::update_fai_stat($campaign_id, $email, 'click');
	}
}
