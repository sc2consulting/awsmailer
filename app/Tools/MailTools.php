<?php


namespace App\Tools;


use App\Models\FaiList;
use App\Models\recipient_email;
use App\Models\RecipientEmail;
use App\Models\SendingQueue;
use Illuminate\Support\Facades\Log;

class MailTools
{
	public static function filterStringsWithList($array, $faiList)
	{
		$filteredList = array();
		foreach ($array as $elem) {
			if (strpos($elem, '@') !== false) {
				$fai = '@' . explode('@', $elem)[1];
				if (!in_array($fai, $faiList)) {
					array_push($filteredList, $elem);
				}
			}
		}
		return $filteredList;
	}

	public static function getFai($email)
	{
		if (strpos($email, '@') === false) {
			Log::error('Value ' . $email . ' is not an email');
		}
		return explode('@', $email)[1];
	}

	public static function isEmail($email)
	{
		$atPos = strpos($email, '@');
		$dotPos = strpos($email, '.', $atPos);
		return ($atPos !== false && $dotPos !== false && $atPos < $dotPos);
	}
}