<?php



namespace App\Tools;


class Encryptor
{
	public static function simple_encrypt($string)
	{
		$token = self::stringGenerator();

		$salt = env('salt');
		$salt = str_pad($salt, strlen($token), $salt);

		$string = str_pad($string, strlen($token));

		$key = ($salt ^ $token);
		return $token . "." . base64_encode($key ^ $string); // return cipher
	}

	public static function simple_decrypt($string)
	{
		$string = explode('.', $string);
		$token = $string[0];
		$cipher = base64_decode($string[1]);

		$salt = env('salt');
		$salt = str_pad($salt, strlen($token), $salt);

		$key = $salt ^ $token;
		return ($cipher ^ $key);
	}

	public static function stringGenerator()
	{
		$randString = strval(time() * getmypid());
		return hash('sha256', $randString);
	}
}