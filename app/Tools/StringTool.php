<?php


namespace App\Tools;


class StringTool
{
	static public function htmlCharConverter($string) {
		$char = array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','Þ','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ð','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','þ','ÿ');
		$htmlChar = array('&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;', '&AElig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;', '&ecirc;', '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;' ,'&yuml;');

		return str_replace($char, $htmlChar, $string);
	}
}

/*
array(
	'À' => "&Agrave;",
	'Á' => "&Aacute;",
	'Â' => "&Acirc;",
	'Ã' => "&Atilde;",
	'Ä' => "&Auml;",
	'Å' => "&Aring;",
	'Æ' => "&AElig;",
	'Ç' => "&Ccedil;",
	'È' => "&Egrave;",
	'É' => "&Eacute;",
	'Ê' => "&Ecirc;",
	'Ë' => "&Euml;",
	'Ì' => "&Igrave;",
	'Í' => "&Iacute;",
	'Î' => "&Icirc;",
	'Ï' => "&Iuml;",
	'Ð' => "&ETH;",
	'Ñ' => "&Ntilde;",
	'Ò' => "&Ograve;",
	'Ó' => "&Oacute;",
	'Ô' => "&Ocirc;",
	'Õ' => "&Otilde;",
	'Ö' => "&Ouml;",
	'Ø' => "&Oslash;",
	'Ù' => "&Ugrave;",
	'Ú' => "&Uacute;",
	'Û' => "&Ucirc;",
	'Ü' => "&Uuml;",
	'Ý' => "&Yacute;",
	'Þ' => "&THORN;",
	'ß' => "&szlig;",
	'à' => "&agrave;",
	'á' => "&aacute;",
	'â' => "&acirc;",
	'ã' => "&atilde;",
	'ä' => "&auml;",
	'å' => "&aring;",
	'æ' => "&aelig;",
	'ç' => "&ccedil;",
	'è' => "&egrave;",
	'é' => "&eacute;",
	'ê' => "&ecirc;",
	'ë' => "&euml;",
	'ì' => "&igrave;",
	'í' => "&iacute;",
	'î' => "&icirc;",
	'ï' => "&iuml;",
	'ð' => "&eth;",
	'ñ' => "&ntilde;",
	'ò' => "&ograve;",
	'ó' => "&oacute;",
	'ô' => "&ocirc;",
	'õ' => "&otilde;",
	'ö' => "&ouml;",
	'ø' => "&oslash;",
	'ù' => "&ugrave;",
	'ú' => "&uacute;",
	'û' => "&ucirc;",
	'ü' => "&uuml;",
	'ý' => "&yacute;",
	'þ' => "&thorn;",
	'ÿ' => "&yuml;"
);
*/