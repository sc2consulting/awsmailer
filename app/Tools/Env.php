<?php


namespace App\Tools;


use Illuminate\Support\Facades\Log;

class Env
{
	public static function getEnvFromFile($filename)
	{
		$filepath = storage_path() . DIRECTORY_SEPARATOR . $filename;
		if (!file_exists($filepath)) {
			Log::error('Could not find Env file in ' . $filepath . PHP_EOL);
		}

		$fh = fopen($filepath, 'r');
		$keys = array();
		while ($line = fgets($fh)) {
			if (strpos($line, '=')) {
				$elem = explode('=', trim($line));
				$keys[$elem[0]] = $elem[1];
			}
		}
		return $keys;
	}

	public static function getKeyFromFile($filename, $key)
	{
		return self::getEnvFromFile($filename)[$key];
	}
}
