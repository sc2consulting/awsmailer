<?php


namespace App\Tools;


class FileManager
{
	/**
	 * reads given file from Laravel Storage and trims ";' as well
	 *
	 * @param $pathInStorage
	 * @return array
	 */
	public static function lineToArray($pathInStorage)
	{
		$filename = storage_path() . $pathInStorage;
		if (!file_exists($filename)) {
			return array();
		}
		$fh = fopen($filename, 'r');

		$list = array();
		while ($line = fgets($fh)) {
			$mail = trim($line);
			$mail = trim($mail, ';');
			array_push($list, $mail);
		}
		return $list;
	}

	public static function write_in_storage($filepath, $filename, $content)
	{
		return file_put_contents(storage_path() . "/" . $filepath . "/" . $filename, $content);
	}

	/**
	 * check if file exists and returns its content if it exist, return false otherwise
	 * @param $pathInStorage
	 * @return array|false|string
	 */
	public static function getContents($pathInStorage)
	{
		$filename = storage_path() . $pathInStorage;
		if (!file_exists($filename)) {
			return false;
		}
		return file_get_contents($filename);
	}
}