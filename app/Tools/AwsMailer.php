<?php


namespace App\Tools;


use App\Models\FaiList;
use App\Models\RecipientEmail;
use App\Models\Sender;
use App\Models\sentEmail;
use Aws\Ses\Exception\SesException;
use Aws\Ses\SesClient;
use Html2Text\Html2Text;
use Illuminate\Support\Facades\Log;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;

class AwsMailer
{
	/**
	 * Aws
	 * @var array
	 */
	private $key;

	public function __construct()
	{
		$this->key = Env::getEnvFromFile('aws.key');
	}

	/**
	 * Only one at the time, otherwise click notification won't work
	 * @param $campaign_id
	 * @param $subject
	 * @param $email
	 * @param $htmlBody
	 * @param $sender
	 * @return bool
	 */
	public function sendMailSES($campaign_id, $subject, $email, $htmlBody, $sender)
	{
		$sesClient = new SesClient([
			'credentials' => [
				'key'    => $this->key['AWS_S3_READER_KEY_ID'],
				'secret' => $this->key['AWS_S3_READER_SECRET']
			],
			'region' => 'us-west-2',
			'version' => 'latest'
		]);

		$htmlBody = str_replace('$VARIABLEEMAIL', Encryptor::simple_encrypt($email), $htmlBody);

		$html2txt = new Html2Text($htmlBody);

		$param = [
			'ConfigurationSetName' => 'openClickNotif',
			'Destination' => [ // REQUIRED
				'ToAddresses' => [$email],
			],
			'Message' => [ // REQUIRED
				'Body' => [ // REQUIRED
					'Html' => [
						'Charset' => 'utf-8',
						'Data' => $htmlBody, // REQUIRED
					],
					'Text' => [
						'Charset' => 'utf-8',
						'Data' => ($html2txt->getText()), // REQUIRED
					],
				],
				'Subject' => [ // REQUIRED
					'Charset' => 'utf-8',
					'Data' => $subject, // REQUIRED
				],
			],
			'Source' => $sender->name . " <" . $sender->email . ">", // REQUIRED
//			'SourceArn' => '',
//			'Tags' => [
//				[
//					'Name' => '<string>', // REQUIRED
//					'Value' => '<string>', // REQUIRED
//				],
				// ...
//			],
		];
		try {
			$res = $sesClient->sendEmail($param);
			$newMail = new SentEmail;
			$newMail->email = $email;
			$newMail->campaign_id = $campaign_id;
			$newMail->aws_message_id = $res->get('MessageId');
			$newMail->fai_id = FaiList::getFaiId($email);
			$newMail->save();
			return true;
		} catch (SesException $e) {
			echo 'SES Error :' . $e->getAwsErrorMessage() . PHP_EOL;
			return false;
		}
	}

	public function sendTestMail($email, $subject, $htmlBody, $sender ,$displayName)
	{
		$sesClient = new SesClient([
			'credentials' => [
				'key'    => $this->key['AWS_S3_READER_KEY_ID'],
				'secret' => $this->key['AWS_S3_READER_SECRET']
			],
			'region' => 'us-west-2',
			'version' => 'latest'
		]);

		$htmlBody = str_replace('$VARIABLEEMAIL', $email, $htmlBody);
		$htmlBody = str_replace('$UNSUBEMAILLINK', 'https://e.desabaz.com/news/unsub?token=' . Encryptor::simple_encrypt($email), $htmlBody);

		$html2txt = new Html2Text($htmlBody);

		$param = [
			'ConfigurationSetName' => 'openClickNotif',
			'Destination' => [ // REQUIRED
				'ToAddresses' => [$email],
			],
			'Message' => [ // REQUIRED
				'Body' => [ // REQUIRED
					'Html' => [
						'Charset' => 'utf-8',
						'Data' => $htmlBody, // REQUIRED
					],
					'Text' => [
						'Charset' => 'utf-8',
						'Data' => ($html2txt->getText()), // REQUIRED
					],
				],
				'Subject' => [ // REQUIRED
					'Charset' => 'utf-8',
					'Data' => $subject, // REQUIRED
				],
			],
			'Source' => $displayName . " <" . $sender . ">", // REQUIRED
//			'SourceArn' => '',
//			'Tags' => [
//				[
//					'Name' => '<string>', // REQUIRED
//					'Value' => '<string>', // REQUIRED
//				],
			// ...
//			],
		];

		try {
			$sesClient->sendEmail($param);
			return true;
		} catch (SesException $e) {
			Log::info("ERROR $e ". PHP_EOL);
			echo "ERROR $e ". PHP_EOL;
			return false;
		}
	}

	public function sendMailSESAsync($campaign_id, $subject, $email, $htmlBody, $sender) {
		$sesClient = new SesClient([
			'credentials' => [
				'key'    => $this->key['AWS_S3_READER_KEY_ID'],
				'secret' => $this->key['AWS_S3_READER_SECRET']
			],
			'region' => 'us-west-2',
			'version' => 'latest'
		]);

		$htmlBody = str_replace('$VARIABLEEMAIL', Encryptor::simple_encrypt($email), $htmlBody);

		$html2txt = new Html2Text($htmlBody);

		$param = [
			'ConfigurationSetName' => 'openClickNotif',
			'Destination' => [ // REQUIRED
				'ToAddresses' => [$email],
			],
			'Message' => [ // REQUIRED
				'Body' => [ // REQUIRED
					'Html' => [
						'Charset' => 'utf-8',
						'Data' => $htmlBody, // REQUIRED
					],
					'Text' => [
						'Charset' => 'utf-8',
						'Data' => ($html2txt->getText()), // REQUIRED
					],
				],
				'Subject' => [ // REQUIRED
					'Charset' => 'utf-8',
					'Data' => $subject, // REQUIRED
				],
			],
			'Source' => $sender->name . " <" . $sender->email . ">", // REQUIRED
//			'SourceArn' => '',
//			'Tags' => [
//				[
//					'Name' => '<string>', // REQUIRED
//					'Value' => '<string>', // REQUIRED
//				],
			// ...
//			],
		];

//		echo "sending mail to " . $email . "...";
		$promise = $sesClient->sendEmailAsync($param);
		$promise->then(
			function ($value) use ($email, $campaign_id) {
				$newMail = new SentEmail;
				$newMail->email = $email;
				$newMail->campaign_id = $campaign_id;
				$newMail->aws_message_id = $value->get('MessageId');
				$newMail->fai_id = FaiList::getFaiId($email);
				$newMail->save();
				echo " > Email sent to $email." . PHP_EOL;
				return true;
			},
			function ($reason) use ($email) {
				echo "$email > Error during mail sending :" . $reason . PHP_EOL;
				file_put_contents(storage_path() . "/logs/test.txt", "$email > Error during mail sending :" . $reason->getMessage() . PHP_EOL, FILE_APPEND);
				return false;
			}
		);
		return $promise;
	}
}