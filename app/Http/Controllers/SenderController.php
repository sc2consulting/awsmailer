<?php

namespace App\Http\Controllers;

use App\Models\Sender;
use App\Tools\AwsMailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SenderController extends Controller
{
	public function create(Request $request)
	{
		if (!$request->has('email') || !$request->has('test_recipient')) {
			return "the form was not complete";
		}

		$sender_email = $request->email;
		$test_recipient = $request->test_recipient;
		if (Sender::where('email', $sender_email)->first() != NULL) {
			return "This sender already exists.";
		}
		$mailer = new AwsMailer();
		$res = $mailer->sendTestMail(
			$test_recipient,
			'sc2TestMailer',
			file_get_contents(storage_path() . '/campaign/html/mailSenderVerification.html'),
			$sender_email,
			'awsMailer test'
		);
		if ($res) {
			$newSender = new Sender();
			$newSender->email = $sender_email;
			$newSender->aws_id = "";
			$newSender->save();
			$pageSize = $request['nbElem'] ?: 15;
			$senderList = DB::table('sender')
				->orderBy('created_at', 'desc')
				->paginate($pageSize);
			return view('sender.index', ['senderList' => $senderList]);
			}
		return "the verification failed, please verify given values.";
	}

    public function senderListView(Request $request)
    {
	    $pageSize = $request['nbElem'] ?: 15;
	    $senderList = DB::table('sender')
		    ->orderBy('created_at', 'desc')
		    ->paginate($pageSize);
	    return view('sender.index', ['senderList' => $senderList]);
    }

	public function update()
	{
		//
	}

	public function delete(Request $request)
	{
//		if (!$request->has('sender_id')
//			|| ($sender = Sender::where('id', $request->sender_id)->first()) == NULL) {
//			return "invalid or unknown sender ID.";
//		}
//		$sender = Sender::where('id', $request->has('sender_id'));
//		if ($sender->)
	}
	public function createView()
	{
		return view('sender.createView');
	}
}
