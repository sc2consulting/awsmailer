<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\FaiList;
use App\Models\RecipientEmail;
use App\Models\Sender;
use App\Models\SendingQueue;
use App\Tools\AwsMailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CampaignController extends Controller
{
	const html_dir = "campaign" . DIRECTORY_SEPARATOR . "html";
	const mail_dir = "campaign" . DIRECTORY_SEPARATOR . "mailList";

    public function create(Request $request)
    {
        $requiredKeys = ['name', 'sender_id', 'sender_name', 'mail_subject', 'mail_html'];
        foreach ($requiredKeys as $key) {
            if (!$request[$key]) {
                return "missing $key" . PHP_EOL;
            }
		}

        if (Sender::where('id', $request->sender_id)->first() === NULL) {
        	return "unknown sender_id";
        }

        $filename = date("Y-m-d_H:i:s") . "_" . $request->file('mail_html')->getClientOriginalName();
//	    $filepath = $request->file('mail_html')->storeAs("campaign/html/", $filename);
	    $request->file('mail_html')->storeAs(self::html_dir, $filename);

        $newCampaign = new Campaign;
        $newCampaign->name = $request->name;
//        $newCampaign->tag = $request->tag ?: "";
        $newCampaign->sender_id = $request->sender_id;
        $newCampaign->sender_name = $request->sender_name;
        $newCampaign->mail_subject = $request->mail_subject;
		$newCampaign->mail_body = $filename;
        $newCampaign->save();
        return redirect('/campaign');
    }

    public function update(Request $request)
    {
	    $changeList = $request->all();
	    if (!array_key_exists('campaign_id', $changeList)) {
	    	return "no campaign ID given.";
	    }
	    $campaign = Campaign::where('id', $changeList['campaign_id'])->first();

	    if (array_key_exists('campaign_name', $changeList) && $changeList['campaign_name'] != NULL)
		    $campaign->name = $changeList['campaign_name'];
	    if (array_key_exists('sender_id', $changeList) && $changeList['sender_id'] != NULL)
			$campaign->sender_id = $changeList['sender_id'];
	    if (array_key_exists('sender_name', $changeList) && $changeList['sender_name'] != NULL)
			$campaign->sender_name = $changeList['sender_name'];
	    $campaign->save();
	    return "ok";
    }

    public function delete(Request $request)
    {
	    if (!$request->has('campaign_id')) {
		    Log::info("CampaignController::Delete : no ID given.");
	    }
	    $campaign_id = $request->get('campaign_id');

	    $campaign = Campaign::where('id', $campaign_id)->first();
	    if ($campaign == NULL) {
		    Log::info("Attempt in deleting nonexistent campaign with id $campaign_id.");
		    return 0;
	    }
	    if ($campaign->email_sent != 0) {
		    Log::info("Cannot remove Campaign #$campaign_id, email have been sent for this campaign.");
		    return 0;
		    return 0;
	    }
	    $campaign->deleteWithQueue();
	    return redirect('/campaign');

    }

	public function campaignListView(Request $request)
	{
		$campaigns = Campaign::all();
		$pendingList = [];
		foreach ($campaigns as $campaign) {
			$pendingList[$campaign->id] = SendingQueue::countByCampaignID($campaign->id);
		}
		$pageSize = $request['nbElem'] ?: 15;
		$campaignList = DB::table('campaign')
			->orderBy('created_at', 'desc')
			->paginate($pageSize);
		return view('campaign.index', ['campaignList' => $campaignList, 'pending_list' => $pendingList]);
	}

	public function detailView(Request $request)
	{
		if (!$request['id']) {
			return view('errors.500');
		}
		$id = $request['id'];

		$campaign = Campaign::where('id', $id)->first();
		$sender = Sender::where('id', $campaign->sender_id)->first();
		$campaignInfo = array(
			'id' => $campaign->id,
			'name' => $campaign->name,
			'sender' => $sender->name,
			'subject' => $campaign->mail_subject,
			'htmlFile' => $campaign->mail_body,
			'is_active' => $campaign->isActive ? "yes" : "no"
		);

		$fai_stats = json_decode($campaign->fai_stats, true);
		$stats = array();
		if ($fai_stats) {
			$keyList = array_keys($fai_stats);
			foreach ($keyList as $key) {
				$stats [] = [
					'name' => FaiList::where('id', $key)->first()->name,
					'sent' => key_exists('sent', $fai_stats[$key]) ? $fai_stats[$key]['sent'] : 0,
					'delivery' => key_exists('delivery', $fai_stats[$key]) ? $fai_stats[$key]['delivery'] : 0,
					'open' => key_exists('open', $fai_stats[$key]) ? $fai_stats[$key]['open'] : 0,
					'click' => key_exists('click', $fai_stats[$key]) ? $fai_stats[$key]['click'] : 0,
					'bounce' => key_exists('bounce', $fai_stats[$key]) ? $fai_stats[$key]['bounce'] : 0,
					'complaint' => key_exists('complaint', $fai_stats[$key]) ? $fai_stats[$key]['complaint'] : 0,
				];
			}
		}

		return view('campaign.detailView', [
			'campaign' => $campaignInfo,
			'stats' => $stats
		]);
	}

	public function createView()
	{
		$senderList = Sender::all();
		$baseList = scandir(storage_path() . "/app/campaign/html/");
		return view('campaign.createView', [
			'senders' => $senderList,
			'baseList' => $baseList
		]);
	}

	public function editView(Request $request)
	{
		if (!$request['id']) {
			return view('errors.500');
		}
		$campaign = Campaign::where('id', $request['id'])->first();
		return view('campaign.editView', [
			'name' => $campaign->name,
			'sender_id' => $campaign->sender_id,
			'sender_name' => $campaign->sender_name,
			'mail_subject' => $campaign->mail_subject,
			'mail_body' => $campaign->mail_body
		]);
	}

	public function testCampaign(Request $request)
	{
		if (env('test_sender_email') == NULL) {
			return "No test email found, make sure to have 'test_sender_email' set in .env";
		}
		if (!isset($request->campaign_id)) {
			return "no id given to test the campaign." . PHP_EOL;
		}
		if (!isset($request->email)) {
			return "no recipient given to test the mail." . PHP_EOL;
		}
		$campaign = Campaign::where('id', $request->campaign_id)->first();
		if ($campaign === NULL) {
			return "campaign not found";
		}
		$html = file_get_contents(storage_path() . "/app/campaign/html/" . $campaign->mail_body);
		$mailer = new AwsMailer();
		echo "sending a test email to " . $request['email'] . PHP_EOL;
		if ($mailer->sendTestMail($request->email, $campaign->mail_subject, $html, env('test_sender_email'), $campaign->sender_name))
			echo"A test email has been sent." . PHP_EOL;
		else
			echo "An error occurred while sending the test Email.";
		sleep(3);
		return redirect('/campaign');
	}

	public function addRecipientForm(Request $req)
	{
		$campaign_id = $req['campaign_id'];
		if (!$campaign_id) {
			return "error while fetching campaign ID (not found)";
		}
		return view('campaign.element.addRecipientForm', [
			'campaign_id' => $campaign_id,
			'files' => scandir(storage_path() . '/app/campaign/mailList/')
			]
		);
	}

	public function addRecipient(Request $request)
	{
		$startTime = time();
		if (!($campaign_id = $request['campaign_id'])) {
			return "no campaign_id given." . PHP_EOL;
		}
		$filename = $request['existingFile'];
		if (!$filename || $filename === 'none') {
			if (!$request['mail_file']) {
				return 'no file given' . PHP_EOL;
			}

			$filename = date("Y-m-d_H:i:s") . "_" . $request->file('mail_file')->getClientOriginalName();
			$filepath = $request->file('mail_file')->storeAs(self::mail_dir, $filename);
		}


		$add_result = SendingQueue::addFileList($filename, $campaign_id);
		$time = time() - $startTime;
		return view('campaign.addRecipientResult', ['time' => $time, 'stats' => $add_result]);
	}

	public function showQueue(Request $req)
	{
		$campaign_id = $req['campaign_id'] ?: $req['id'];
		if (!$campaign_id) {
			echo "no campaign id given";
			return false;
		}
		$campaign = Campaign::where('id', $campaign_id)->first();
		if (!$campaign) {
			echo "no campaign with given ID was found";
			return false;
		}
		$sendList = SendingQueue::where('campaign_id', $campaign_id)->get();

		$list = array();
		foreach ($sendList as $elem) {
			$recipient = RecipientEmail::where('email', $sendList->email)->first();
			$list []= [
				'id' => $elem->id,
				'email_id' => $recipient->id,
				'email' => $recipient->email,
				'fai' => FaiList::where('id', $recipient->fai_id)->first()
			];
		}

		return view('campaign.element.table_sendingQueue', [
			'queueList' => $list,
			'campaign' => $campaign
			]
		);
	}
}