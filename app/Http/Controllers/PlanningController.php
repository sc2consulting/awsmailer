<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use App\Models\CampaignPlanning;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PlanningController extends Controller
{
//	todo : timestamp timezone
	public function planningView(Request $req)
	{
		$plannings = DB::table('campaign_planning')
			->orderBy('created_at', 'desc')
			->paginate($req['pageSize'] ?: 10);
		return view('planning.index', ['plannings' => $plannings]);
	}

	public function planningDetail(Request $req)
	{
		$planning = CampaignPlanning::where('id', $req->id)->first();
		// todo : stats total + pecent
		return view('planning.detailView', ['planning' => $planning]);
	}

	public function createView()
	{
		$campaigns = Campaign::orderBy('id', 'desc')->take(10)->get();
		return view('planning.create', ['campaigns' => $campaigns, 'date' => date("Y-m-d\TH:i")]);
	}

	public function create(Request $req)
	{
		$params = ['campaign_id', 'sending_date'];
		foreach ($params as $key) {
			if (!isset($req->$key)) {
				return "missing $key" . PHP_EOL;
			}
		}

		$campaign = Campaign::where('id', $req->campaign_id)->first();

		$newPlanning = new CampaignPlanning();
		$newPlanning->campaign_id = $campaign->id;
		$newPlanning->sending_date = $req->sending_date;
		$newPlanning->active = $req->active ?: false;
		$newPlanning->save();
		return redirect('/planning');
	}

	public function updateView(Request $req)
	{
		if (!isset($req->campaign_id)) {
			return "missing id parameter" . PHP_EOL;
		}
		$planning = CampaignPlanning::where('id', $req->campaign_id);
		return view('planning.update', ['planning' => $planning]);
	}

	public function update(Request $req)
	{
		if (!isset($req->planning_id)) {
			return "missing id to fetch planning";
		}
		$planning = CampaignPlanning::where('id', $req['planning_id'])->first();
//		$campaign = Campaign::where('id', $planning->campaign_id);
		if (isset($req->sending_date)) {
			$date = str_replace('T', ' ', $req->sending_date).':00';
			$planning->sending_date = $date;
		}
		$planning->save();
		return redirect('/planning');
	}

	public function toggleActive(Request $req)
	{
		if (!isset($req->planning_id)) {
			return "no planning_id was given";
		}
		$planning = CampaignPlanning::where('id', $req->planning_id)->first();
		$planning->active = !($planning->active);
		$planning->save();
		return redirect('/planning');
	}

	public function delete(Request $req)
	{
		if (!isset($req->planning_id)) {
			return "missing planning_id";
		}
		$planning = CampaignPlanning::where('id', $req['planning_id'])->first();
		$planning->delete();
		return redirect('/planning');
	}
}
