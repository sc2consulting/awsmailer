<?php


namespace App\Http\Controllers;


use App\Console\Commands\AwsSESNotification;
use App\Models\Campaign;
use App\Models\RecipientEmail;
use App\Tools\Encryptor;
use Illuminate\Http\Request;

class AwsMailController extends Controller
{
    public function getReceiptMail(Request $request)
    {
        $content = $request->all();
        if (key_exists('email', $content)) {
            var_dump($content);
            return true;
        }
        print('No mail found.' . PHP_EOL);
        return false;
    }

    public function unsubscribe(Request $request)
    {
	    if (strpos($request->url(), 'http://127.0.0.1:8000/news/unsub') !== 0
	        || !$request->has('token')) {
		    return view('errors.404');
	    }

	    $email = Encryptor::simple_decrypt($request->token);

	    if (($recipient = RecipientEmail::where('email', $email)->first()) === NULL) {
		    return view('errors.404');
	    }
	    $recipient->unsubscribe = true;
	    $recipient->save();
	    return view('unsubscribe.ok', ['email' => $email]);
    }
}
