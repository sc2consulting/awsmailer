<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Deletes works only if the element has recently been created ( never used )
 */

//Route::get('/getMail', [AwsMailController::class, 'getReceiptMail']);


//Route::get('/login', [Controllers\AuthController::class, 'login'])->name('login');

Route::middleware(['auth'])->group(function () {

	/*
	 * sender
	 */
	Route::get('/sender', [Controllers\SenderController::class, 'senderListView']);
	Route::get('/sender/create', [Controllers\SenderController::class, 'createView']);
	Route::post('/sender/create', [Controllers\SenderController::class, 'create']);
	//Route::post('/sender/remove', [Controllers\SenderController::class, 'remove']);


	/*
	 * campaign
	 */
	Route::get('/campaign', [Controllers\CampaignController::class, 'campaignListView']);
	Route::get('/campaign/detail', [Controllers\CampaignController::class, 'detailView']);

	Route::get('/campaign/create', [Controllers\CampaignController::class, 'createView']);
	Route::post('/campaign/create', [Controllers\CampaignController::class, 'create']);
	Route::post('/campaign/testCampaign', [Controllers\CampaignController::class, 'testCampaign']);

	Route::post('/campaign/addRecipient', [Controllers\CampaignController::class, 'addRecipient']);
	Route::get('/campaign/addRecipientForm', [Controllers\CampaignController::class, 'addRecipientForm']);

	Route::get('/campaign/showQueue', [Controllers\CampaignController::class, 'showQueue']);

	Route::post('/campaign/delete', [Controllers\CampaignController::class, 'delete']);


	/*
	 * Planning
	 */
	Route::get('/planning', [Controllers\PlanningController::class, 'planningView']);

	Route::get('/planning/create', [Controllers\PlanningController::class, 'createView']);
	Route::post('/planning/create', [Controllers\PlanningController::class, 'create']);

	Route::get('/planning/edit', [Controllers\PlanningController::class, 'updateView']);
	Route::post('/planning/update', [Controllers\PlanningController::class, 'update']);
	Route::post('/planning/activate', [Controllers\PlanningController::class, 'toggleActive']);

	Route::post('/planning/delete', [Controllers\PlanningController::class, 'delete']);
	Route::get('/home', [Controllers\CampaignController::class, 'campaignListView']);


	/*
	 * Recipient
	 */
	Route::get('/recipientFiles', [Controllers\RecipientController::class, 'getRecipientFileList']);
});

Route::get('/', [Controllers\HomeController::class, 'index']);

Route::get('/news/unsub/', [Controllers\AwsMailController::class, 'unsubscribe'])->withoutMiddleware(['auth']);
Auth::routes();
